import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import mongoose from "mongoose";


// routes

import ChatRoute from './routes/ChatRoute.js'
import MessageRoute from './routes/MessageRoute.js'

const app = express();



app.use(cors());



dotenv.config();
const PORT = process.env.PORT||3001;

const CONNECTION =process.env.MONGODB_CONNECTION||"mongodb://mongo:27017/chat_service";
mongoose
  .connect(CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("mongo connected"))
  .catch((error) => console.log(`${error} did not connect`));




app.use('/chats', ChatRoute);
app.use('/messages', MessageRoute);

app.listen(PORT, () => console.log(`Listening at Port ${PORT}`))