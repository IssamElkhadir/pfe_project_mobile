import express from 'express'
import { createChat, findChat, userChats } from '../controllers/ChatController.js';
const router = express.Router()

router.get('/get', (req,res)=>{
    res.send('test hello');
});
router.post('/add', createChat);
router.get('/:userId', userChats);
router.get('/find/:firstId/:secondId', findChat);

export default router