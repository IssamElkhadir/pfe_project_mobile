import express from "express";
import { getStudentResults  ,getCorrectedQuestions} from "../controllers/studentResults.js";

const router = express.Router();

router.route("/").post(getStudentResults)
router.route("/get_corrected_questions").post(getCorrectedQuestions)

export default router;