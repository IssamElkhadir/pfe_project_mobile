import express from "express";
import {getStudentResponses , saveAnswers} from "../controllers/studentResponses.js"
const router = express.Router();

router.route("/quiz_passing/studentResponses").post(getStudentResponses)
router.route("/quiz_passing/saveAnswers").post(saveAnswers)

export default router;