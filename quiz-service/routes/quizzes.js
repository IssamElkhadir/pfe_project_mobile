
import express from "express";
import {getQuizzes_student , saveDuration, saveStartedQuiz , saveQuizResult , deleteQuizSession} from "../controllers/quizzes.js";
const router = express.Router();


router.route("/").post(getQuizzes_student)
router.route("/delete_quiz_session_by_student").post(deleteQuizSession)

router.route('/quiz_passing/saveStartedQuiz').post(saveStartedQuiz);
router.route('/quiz_passing/saveDuration').post(saveDuration);
router.route('/quiz_passing/saveQuizResult').post(saveQuizResult);


export default router;