
export const connect_to_Database = (connection) => {
    connection.connect((err) => {
        if (err) {
            console.error(`Error connecting to MySQL database: ${err}`);
        } else {
            console.log(`Connected to MySQL database on port : ${connection.config.port.yellow}`);
        }
    });
    connection.on("error", (err) => {
        console.error("MySQL database error:", err);
    });
    
}
