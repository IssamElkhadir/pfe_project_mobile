import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import morgan from "morgan";
import cors from "cors";
import colors from "colors";
import dotenv from "dotenv";
import mysql from "mysql";
import {connect_to_Database} from "./config/index.js";
import router_quizzes from "./routes/quizzes.js";
import router_questions from "./routes/questions.js";
import router_studentResponses from "./routes/studentResponses.js"
import router_studentResults from "./routes/studentResults.js";


// GET ENVIRONMENT VARIABLES
dotenv.config();
const PORT = process.env.SERVER_PORT || 3005;


// APP CONFIGURATION
const app = express();
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({policy : "cross-origin"}));
app.use(morgan("common"));
app.use(express.json({limit : "50mb"}));
app.use(express.urlencoded({limit : "50mb" , extended : true}));
app.use(bodyParser.json({limit : "50mb"}));
app.use(bodyParser.urlencoded({limit : "50mb" , extended : true}));
app.use(cors())

// MYSQL CONNECTION
export const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});
connect_to_Database(connection);

// APP ROUTES 
app.use('/api/quizzes' , router_quizzes)
app.use('/api/quizzes' , router_studentResponses)
app.use('/api/questions' , router_questions)
app.use('/api/student_results' , router_studentResults)

app.get('/', (req, res) => {
    res.send('Hello from Nginx on port 3005!');
});

// SERVER
app.listen(PORT, () => {
    console.log(`Server started on Port: ${PORT.cyan}`);
});