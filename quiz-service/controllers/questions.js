import { connection } from "../server.js";

// Helper function to execute SQL query
const executeQuery = (query, connection) => {
    return new Promise((resolve, reject) => {
        connection.query(query, (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
};


export const getQuestions_student = async (req, res) => {
    try {
        const { quizId, isRandomQuestions, isRandomAnswers } = req.body;
        const questionsQuery = `
            SELECT questions.*
            FROM questions
            INNER JOIN question_by_quiz ON questions.id = question_by_quiz.question_id
            WHERE question_by_quiz.quiz_id = ${quizId}
        `;

        const optionsQuery = `
            SELECT *
            FROM options
            WHERE question_id IN (SELECT question_id FROM question_by_quiz WHERE quiz_id = ${quizId})
        `;

        const questions = await executeQuery(questionsQuery, connection);
        const options = await executeQuery(optionsQuery, connection);

        if (isRandomQuestions === 1) {
            questions.sort(() => Math.random() - 0.5);
        }

        const questionOptions = {};
        options.forEach((option) => {
            const questionId = option.question_id;
            if (!questionOptions[questionId]) {
                questionOptions[questionId] = [];
            }
            questionOptions[questionId].push(option);
        });

        if (isRandomAnswers === 1) {
            Object.values(questionOptions).forEach((options) => {
                options.sort(() => Math.random() - 0.5);
            });
        }
        return res.status(200).json({ questions, questionOptions });

    } catch (error) {
        console.error('Error executing SQL query:', error);
        return res.status(500).json({ message: error.message });
    }
}