import { connection } from "../server.js";

// Helper function to execute SQL query
const executeQuery = (query, connection) => {
    return new Promise((resolve, reject) => {
        connection.query(query, (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
};

export const getStudentResults = async (req, res) => {
    try {
        const { user_id } = req.body;
        const query = `
            SELECT qs.*,
                COALESCE(correct_count.correct_questions_count, 0) AS correct_questions_count,
                COALESCE(incorrect_count.incorrect_questions_count, 0) AS incorrect_questions_count,
                COUNT(DISTINCT qbq.question_id) AS total_questions_count,
                quizzes.quiz_name AS quiz_name
            FROM quizsession AS qs
            LEFT JOIN posts ON posts.id = qs.post_id
            LEFT JOIN quizzes ON quizzes.id = posts.quiz_id
            LEFT JOIN question_by_quiz AS qbq ON quizzes.id = qbq.quiz_id
            LEFT JOIN (
                SELECT quiz_session_id, COUNT(*) AS correct_questions_count
                FROM studentResponses
                WHERE is_response_correct = 1
                GROUP BY quiz_session_id
            ) AS correct_count ON qs.id = correct_count.quiz_session_id
            LEFT JOIN (
                SELECT quiz_session_id, COUNT(*) AS incorrect_questions_count
                FROM studentResponses
                WHERE is_response_correct = 0
                GROUP BY quiz_session_id
            ) AS incorrect_count ON qs.id = incorrect_count.quiz_session_id
            WHERE qs.student_id = ${user_id} AND qs.completed = 1
            GROUP BY qs.id;
        `;
        const studentResults = await executeQuery(query, connection);
        res.status(200).json({ studentResults })
    } catch (error) {
        console.error('Error executing SQL query:', error);
        return res.status(500).json({ message: error.message });
    }
}


export const getCorrectedQuestions = async (req, res) => {
    try {
        const { user_id, quiz_session_id } = req.body;
        const query = `SELECT DISTINCT sr.correct_answer ,
            sr.is_response_correct ,
            sr.response AS user_response ,
            q.question_text  ,
            sr.question_type ,
            sr.question_id
            FROM studentresponses AS sr
            INNER JOIN questions AS q ON q.id = sr.question_id
            WHERE sr.quiz_session_id = ${quiz_session_id} AND sr.student_id = ${user_id}
        `;
        const correctedQuestions = await executeQuery(query, connection);
        // for 'multiple_choice_oneAnswer', 'multiple_choice_multipleAnswer', 'reorder' questions
        await Promise.all(correctedQuestions.map(async (question) => {
            if (question.question_type === "multiple_choice_oneAnswer") {
                const query1 = `SELECT option_text FROM options WHERE id = ${Number(question.user_response)}`
                const query2 = `SELECT option_text FROM options WHERE id = ${Number(question.correct_answer)}`
                const option_text_for_user_response = await executeQuery(query1, connection)
                const option_text_for_correct_response = await executeQuery(query2, connection)
                question.user_response = option_text_for_user_response[0].option_text || question.user_response;
                question.correct_answer = option_text_for_correct_response[0].option_text || question.correct_answer;
            }else if (question.question_type === "multiple_choice_multipleAnswer") {
                const userResponseIDs = question.user_response.split(',').map(Number);
                const correctAnswerIDs = question.correct_answer.split(',').map(Number);
              
                const query1 = `SELECT option_text FROM options WHERE id IN (${userResponseIDs.join(',')})`;
                const query2 = `SELECT option_text FROM options WHERE id IN (${correctAnswerIDs.join(',')})`;
              
                const userResponseOptions = await executeQuery(query1, connection);
                const correctAnswerOptions = await executeQuery(query2, connection);

                let new_user_response = userResponseOptions.map((option)=> option.option_text)
                let new_correct_answer = correctAnswerOptions.map((option)=> option.option_text)
                question.user_response = new_user_response || question.user_response;
                question.correct_answer = new_correct_answer  || question.correct_answer;
            }else if(question.question_type === "reorder"){
                const userResponseIDs = question.user_response.split(',').map(Number);
                const correctAnswerIDs = question.correct_answer.split(',').map(Number);
                const query = `SELECT option_text FROM options WHERE question_id = ${question.question_id}`;
                const options = await executeQuery(query, connection);
                const options_text = options.map((row) => row.option_text);
                const new_user_response = userResponseIDs.map((id) => options_text[id - 1]);
                const new_correct_answer = correctAnswerIDs.map((id) => options_text[id - 1]);
                question.user_response = new_user_response || question.user_response;
                question.correct_answer = new_correct_answer  || question.correct_answer;

            }
        }));
        console.log(correctedQuestions)
        res.status(200).json({ correctedQuestions: correctedQuestions });
    } catch (error) {
        console.error('Error executing SQL query:', error);
        return res.status(500).json({ message: error.message });
    }
}