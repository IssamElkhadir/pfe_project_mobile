import { connection } from "../server.js";

// Helper function to execute SQL query
const executeQuery = (query, connection) => {
    return new Promise((resolve, reject) => {
        connection.query(query, (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
};
// Helper function to get current date
const currentDate = () =>{
    // Get current date and format it
    let currentDate = new Date();
    let year = currentDate.getFullYear();
    let month = String(currentDate.getMonth() + 1).padStart(2, "0");
    let day = String(currentDate.getDate()).padStart(2, "0");
    let hour = String(currentDate.getHours()).padStart(2, "0");
    let minute = String(currentDate.getMinutes()).padStart(2, "0");
    let second = String(currentDate.getSeconds()).padStart(2, "0");
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}

export const getQuizzes_student = async (req, res) => {
    try {
        const { userId } = req.body;
        const query = `
            SELECT
                quizsession.id AS id,
                quizsession.quiz_session_duration,
                quizsession.score,
                quizsession.completed,
                quizsession.start_at as quizsession_start_at ,
                quizsession.end_at as quizsession_end_at ,
                quizzes.id AS quiz_id,
                quizzes.quiz_name AS quiz_name ,
                quizzes.duration AS duration ,
                posts.start_at,
                posts.end_at,
                posts.isRandomAnswers,
                posts.isRandomQuestions,
                COUNT(question_by_quiz.question_id) AS num_questions
            FROM quizsession
            INNER JOIN posts ON posts.id = quizsession.post_id
            INNER JOIN quizzes ON quizzes.id = posts.quiz_id
            INNER JOIN question_by_quiz ON quizzes.id = question_by_quiz.quiz_id
            WHERE quizsession.student_id = ${userId}
            AND (quizsession.completed = 0 OR quizsession.completed IS NULL)
            AND CURDATE() BETWEEN posts.start_at AND posts.end_at
            GROUP BY quizzes.id, quizsession.id
            LIMIT 0, 1000
        `;
        const results = await executeQuery(query, connection);
        return res.status(200).json({ quizzes: results });
    } catch (error) {
        console.error("Error executing SQL query:", error);
        return res.status(500).json({ message: error.message });
    }
};

export const saveStartedQuiz = async (req, res) => {
    try {
        const { user_id, quiz_session_id, duration } = req.body;
        const query = `
            UPDATE quizsession
            SET quiz_session_duration = ${duration}, completed = 0, start_at = '${currentDate()}', score = 0
            WHERE student_id = ${user_id} AND id = ${quiz_session_id};
        `;

        const result = await executeQuery(query, connection);
        return res.status(200).json({message : "the started quiz is saved"});
    } catch (error) {
        console.error("Error executing SQL query:", error);
        return res.status(500).json({ message: error.message });
    }
};

export const saveDuration = async (req, res) => {
    try {
        const { user_id, quiz_session_id, duration } = req.body;
        const query = `
            UPDATE quizsession
            SET quiz_session_duration = ${duration}
            WHERE student_id = ${user_id} AND id = ${quiz_session_id};
        `;

        await executeQuery(query, connection);
        return res.status(200).json({message : "duration is saved"});
    } catch (error) {
        console.error("Error executing SQL query:", error);
        return res.status(500).json({ message: error.message });
    }
};




export const calculateQuizScore = async (user_id , quiz_session_id) =>{
    const query1 = `SELECT * FROM studentresponses WHERE student_id = ${user_id} AND quiz_session_id = ${quiz_session_id}`;
    const student_responses = await executeQuery(query1, connection);

    const question_scores = student_responses
    .filter((student_response) => student_response.question_type !== "open-ended")
    .map((student_response) => {
        if (student_response.is_response_correct === 1) {
        return 1;
        } else {
        return 0;
        }
    });

    const total_score = question_scores.reduce((acc, curr) => {
    return acc + curr;
    }, 0);

    return Math.round((total_score / question_scores.length) * 100);
}

export const saveQuizResult = async (req , res)=> {
    try {
        const {user_id , quiz_session_id , completed , duration} = req.body ;
        const completed_boolean = JSON.parse(completed) ? 1 : 0;
        const score = await calculateQuizScore(user_id, quiz_session_id);
        let query = `UPDATE quizsession
        SET score = ${score}, quiz_session_duration = ${duration}, completed = ${completed_boolean}, end_at = '${currentDate()}'
        WHERE student_id = ${user_id} AND id = ${quiz_session_id}
        ` 
        await executeQuery(query , connection)
        res.status(200).json({message : `your score is ${score}`})

    } catch (error) {
        console.error("Error executing SQL query:", error);
        return res.status(500).json({ message: error.message });
    }
}



export const deleteQuizSession = async (req , res)=>{
    try {
        const {user_id , quiz_session_id} = req.body;
        const query = `UPDATE quizsession 
            SET completed = 1 , end_at = '${currentDate()}'
            where student_id = ${user_id} and id = ${quiz_session_id} ;
        `;
        await executeQuery(query , connection);
        res.status(200).json({message : "quizsession has been deleted"})
    } catch (error) {
        console.error("Error executing SQL query:", error);
        return res.status(500).json({ message: error.message });
    }
}
