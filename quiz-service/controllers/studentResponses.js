import { connection } from "../server.js";
// Helper function to execute SQL query
const executeQuery = (query, connection) => {
    return new Promise((resolve, reject) => {
        connection.query(query, (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
};
// Helper function to get current date
const currentDate = () =>{
    // Get current date and format it
    let currentDate = new Date();
    let year = currentDate.getFullYear();
    let month = String(currentDate.getMonth() + 1).padStart(2, "0");
    let day = String(currentDate.getDate()).padStart(2, "0");
    let hour = String(currentDate.getHours()).padStart(2, "0");
    let minute = String(currentDate.getMinutes()).padStart(2, "0");
    let second = String(currentDate.getSeconds()).padStart(2, "0");
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}

export const getStudentResponses  = async (req , res)=>{
    try {
        const {user_id , quiz_session_id} = req.body ;
        const query = `SELECT * from studentresponses  WHERE quiz_session_id =${quiz_session_id} 
            AND student_id = ${user_id};
        `
        const formatedStudentResponses = {}
        const studentResponses = await executeQuery(query , connection);
        
        studentResponses.map((item)=>{
            formatedStudentResponses[item.question_id] = {
                question_id: item.question_id,
                question_type: item.question_type,
                quiz_session_id: item.quiz_session_id ,
                response: (item.question_type === "multiple_choice_multipleAnswer" || item.question_type === "reorder" ) ? item.response.split(','): item.response,
            }
        })
        res.status(200).json({studentResponses : formatedStudentResponses})
    } catch (error) {
        console.error("Error executing SQL query:", error);
        res.status(500).json({ message: error.message });
    }
}

export const saveAnswers = async (req, res) => {
    try {
        const { user_id, quiz_session_id, question_id, response, question_type } =
            req.body;
        console.log(req.body)
        if (question_type === "open-ended") {
            const checkIfResponseExistsQuery = `SELECT * FROM StudentResponses
                                   WHERE quiz_session_id = ${quiz_session_id}
                                   AND question_id = ${question_id}
                                   AND student_id = ${user_id}`;

            const responseExists = await executeQuery(
                checkIfResponseExistsQuery,
                connection
            );

            if (responseExists.length > 0) {
                const updateQuery = `UPDATE StudentResponses
                       SET response = '${response}' , updated_by_id = ${user_id} , updated_at = '${currentDate()}'
                       WHERE quiz_session_id = ${quiz_session_id}
                       AND question_id = ${question_id}
                       AND student_id = ${user_id}`;

                await executeQuery(updateQuery, connection);
            } else {
                const insertQuery = `INSERT INTO StudentResponses (quiz_session_id, question_id, student_id, response, is_response_correct, correct_answer, question_type , created_by_id , updated_by_id , created_at)
                       VALUES (${quiz_session_id}, ${question_id}, ${user_id}, '${response}', NULL, NULL, '${question_type}' ,${user_id} , ${user_id} , '${currentDate()}'  )`;

                await executeQuery(insertQuery, connection);
            }
            return res.status(200).json({ message: "Answers saved successfully" });
        } else if (question_type === "fill_in_the_blank") {
            let query1 = `
              SELECT * FROM questions WHERE id = ${question_id}
            `;
            const question = await executeQuery(query1, connection);
            let is_response_correct = false;

            if (question[0].operation === "is exactly") {
                if (response.trim() === question[0].answer.trim()) {
                    is_response_correct = true;
                }
            } else if (question[0].operation === "contains") {
                if (response.toLowerCase().includes(question[0].answer.toLowerCase())) {
                    is_response_correct = true;
                }
            } else if (question[0].operation === "exact number") {
                if (response.trim() === question[0].answer.trim()) {
                    is_response_correct = true;
                }
            }

            const checkIfResponseExistsQuery = `
                SELECT * FROM StudentResponses
                WHERE quiz_session_id = ${quiz_session_id}
                AND question_id = ${question_id}
                AND student_id = ${user_id}
            `;

            const responseExists = await executeQuery(
                checkIfResponseExistsQuery,
                connection
            );

            if (responseExists.length > 0) {
                const updateQuery = `
                    UPDATE StudentResponses
                    SET response = '${response}', is_response_correct = ${is_response_correct} ,updated_by_id = ${user_id} , updated_at = '${currentDate()}'
                    WHERE quiz_session_id = ${quiz_session_id}
                    AND question_id = ${question_id}
                    AND student_id = ${user_id}
                `;
                await executeQuery(updateQuery, connection);
            } else {
                const insertQuery = `
                    INSERT INTO StudentResponses (quiz_session_id, question_id, student_id, response, is_response_correct, correct_answer, question_type , created_by_id , updated_by_id , created_at)
                    VALUES (${quiz_session_id}, ${question_id}, ${user_id}, '${response}', ${is_response_correct}, '${question[0].answer}', '${question_type}' , ${user_id} , ${user_id} , '${currentDate()}')
                `;
                await executeQuery(insertQuery, connection);
            }

            return res.status(200).json({ message: "Answers saved successfully" });
        } else if (question_type === "multiple_choice_oneAnswer") {
            const query1 = `
                SELECT options.id AS option_id, options.is_correct AS is_correct
                FROM questions
                INNER JOIN options ON options.question_id = questions.id
                WHERE questions.id = ${question_id}
            `;
            const options = await executeQuery(query1, connection);
            const correct_option = options.find((option) => option.is_correct === 1);
            let is_response_correct = false;

            if (correct_option.option_id == response) {
                is_response_correct = true;
            }

            const checkIfResponseExistsQuery = `
                SELECT * FROM StudentResponses
                WHERE quiz_session_id = ${quiz_session_id}
                AND question_id = ${question_id}
                AND student_id = ${user_id}
            `;

            const responseExists = await executeQuery(
                checkIfResponseExistsQuery,
                connection
            );

            if (responseExists.length > 0) {
                const updateQuery = `
                    UPDATE StudentResponses
                    SET response = '${response}', is_response_correct = ${is_response_correct} , updated_by_id = ${user_id} , updated_at = '${currentDate()}'
                    WHERE quiz_session_id = ${quiz_session_id}
                    AND question_id = ${question_id}
                    AND student_id = ${user_id}
                `;
                await executeQuery(updateQuery, connection);
            } else {
                const insertQuery = `
                    INSERT INTO StudentResponses (quiz_session_id, question_id, student_id, response, is_response_correct, correct_answer, question_type , created_by_id , updated_by_id , created_at)
                    VALUES (${quiz_session_id}, ${question_id}, ${user_id}, '${response}', ${is_response_correct}, '${correct_option.option_id}', '${question_type}' , ${user_id} , ${user_id} , '${currentDate()}')
                `;
                await executeQuery(insertQuery, connection);
            }

            return res.status(200).json({ message: "Answers saved successfully" });
        } else if (question_type === "multiple_choice_multipleAnswer") {
            let query1 = `SELECT options.id AS option_id, options.is_correct AS is_correct
            FROM questions
            INNER JOIN options ON options.question_id = questions.id
            WHERE questions.id = ${question_id}`;
            const options = await executeQuery(query1, connection);

            const correct_options = options
                .filter((option) => option.is_correct === 1)
                .map((option) => option.option_id);

            let is_response_correct = false;
            if (
                response.sort().join(",") === correct_options.sort().join(",")
            ) {
                is_response_correct = true;
            }

            const checkIfResponseExistsQuery = `
                SELECT * FROM StudentResponses
                WHERE quiz_session_id = ${quiz_session_id}
                AND question_id = ${question_id}
                AND student_id = ${user_id}
            `;

            const responseExists = await executeQuery(
                checkIfResponseExistsQuery,
                connection
            );

            if (responseExists.length > 0) {
                const updateQuery = `
                    UPDATE StudentResponses
                    SET response = '${response}', is_response_correct = ${is_response_correct} , updated_by_id = ${user_id} , updated_at = '${currentDate()}'
                    WHERE quiz_session_id = ${quiz_session_id}
                    AND question_id = ${question_id}
                    AND student_id = ${user_id}
                `;
                await executeQuery(updateQuery, connection);
            } else {
                const insertQuery = `
                    INSERT INTO StudentResponses (quiz_session_id, question_id, student_id, response, is_response_correct, correct_answer, question_type , created_by_id , updated_by_id , created_at)
                    VALUES (${quiz_session_id}, ${question_id}, ${user_id}, '${response.join(",")}', ${is_response_correct}, '${correct_options.join(",")}', '${question_type}',${user_id} , ${user_id} , '${currentDate()}')
                `;
                await executeQuery(insertQuery, connection);
            }

            return res.status(200).json({ message: "Answers saved successfully" });

        } else if (question_type === "reorder") {
            let query1 = `SELECT options.id AS option_id, options.option_order AS option_order
            FROM questions
            INNER JOIN options ON options.question_id = questions.id
            WHERE questions.id = ${question_id}`;
            const options = await executeQuery(query1, connection);

            const correct_order = options.map((option) => option.option_order);


            let is_response_correct = false;
            if (response.join(",") === correct_order.join(",")) {
                is_response_correct = true;
            }

            const checkIfResponseExistsQuery = `
                SELECT * FROM StudentResponses
                WHERE quiz_session_id = ${quiz_session_id}
                AND question_id = ${question_id}
                AND student_id = ${user_id}
            `;

            const responseExists = await executeQuery(
                checkIfResponseExistsQuery,
                connection
            );

            if (responseExists.length > 0) {
                const updateQuery = `
                    UPDATE StudentResponses
                    SET response = '${response}', is_response_correct = ${is_response_correct} , updated_by_id = ${user_id} , updated_at = '${currentDate()}'
                    WHERE quiz_session_id = ${quiz_session_id}
                    AND question_id = ${question_id}
                    AND student_id = ${user_id}
                `;
                await executeQuery(updateQuery, connection);
            } else {
                const insertQuery = `
                    INSERT INTO StudentResponses (quiz_session_id, question_id, student_id, response, is_response_correct, correct_answer, question_type , created_by_id , updated_by_id , created_at)
                    VALUES (${quiz_session_id}, ${question_id}, ${user_id}, '${response.join(",")}', ${is_response_correct}, '${correct_order.join(",")}', '${question_type}',${user_id} , ${user_id} , '${currentDate()}')
                `;
                await executeQuery(insertQuery, connection);
            }

            return res.status(200).json({ message: "Answers saved successfully" });
        }
    } catch (error) {
        console.error("Error executing SQL query:", error);
        res.status(500).json({ message: error.message });
    }
};

