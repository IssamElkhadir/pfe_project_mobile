import { Server } from "socket.io";

const io = new Server({
  cors: {
    origin: "*",
  },
});

let onlineUsers = [];

const addNewUser = (userId, socketId) => {
  !onlineUsers.some((user) => user.userId === userId) &&
    onlineUsers.push({ userId, socketId });
};

const removeUser = (socketId) => {
  onlineUsers = onlineUsers.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return onlineUsers.find((user) => user.userId === userId);
};

io.on("connection", (socket) => {
  socket.on("newUser", (userId) => {
    addNewUser(userId, socket.id); 
    console.log('user added'+userId)
  });

  socket.on("sendNotification", ({ senderName, receiverName, type }) => {
    const receiver = getUser(receiverName);
    io.to(receiver.socketId).emit("getNotification", {
      senderName,
      type,
    });
  });

  socket.on("sendText", (message) => {
    const receiver = getUser(message.receiverId);
    io.to(receiver.socketId).emit("getText", message);
  });

  socket.on("disconnect", () => {
    removeUser(socket.id);
  });
});

io.listen(3002);
