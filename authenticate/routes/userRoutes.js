const express = require("express");
const userControllers = require("../controllers/userControllers.js");
const router = express.Router();

// @route GET && POST - /posts/
router.post("/login", userControllers.signIn)

module.exports = router;