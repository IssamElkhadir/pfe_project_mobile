const User=require('../models/User');
const jwt=require("jsonwebtoken");
const uuid=require('uuid');
const bcrypt =require('bcryptjs')
exports.signIn= async (req,res,next)=>{
    try {
        const {username,password}= req.body;
        if(username&&password){
            const [user,_]= await User.findByUsername(username);
            if (user.length>0){
                bcrypt.compare(password, user[0].password, (error, result) => {
                    
                        if (result) {
                            let token=jwt.sign({'user':user},process.env.JWT_KEY)
                            res.json({ message:"success",token:token,user:user[0] ,status:200});
                        } else {
                            res.json({ message:"incorrect username or password !!!!",status:404 });
                        }
                    
                })
                //return res.json({user:user})
            }
            else res.json({ message:"incorrect username or password !",status:404 });
        }
        else return res.json({ message:"fields are required !",status:300 });
    } catch (error) {
        res.json({ message:"some thing went wrong !!!",status:500,err:error });
    }
}
