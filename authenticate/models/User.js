
const db=require('../config/connection');
const uuid=require('uuid')
class User{
  
    static login(username,password){
        let sql=`select * from users where name=? and password=?`;
        return db.execute(sql,[username,password])
    }
    static findByUsername(username){
        let sql;
        sql="SELECT * FROM users where name=?";
        return db.execute(sql,[username])
    }
    static findAll(){
        let sql=`SELECT * FROM users`;
        return db.execute(sql)
    }
    static findOne(id){
        let sql=`SELECT * FROM users where id_user=?`;
        return db.execute(sql,[id]);
    }
    
    
    
}
module.exports = User;