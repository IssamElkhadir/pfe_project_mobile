const express=require('express');
const dotenv=require('dotenv');


const cors=require('cors');
const userRoutes=require("./routes/userRoutes.js")
dotenv.config();
// express App
const app =express();

// midilleWaires
app.use(cors());

app.use(express.json());


app.use((err,req,res,next)=>{
    if(err){
        console.log(err);
        res.status(500).json({
            message:"some this went wrong !"
        })
    }
})

app.use("/user", userRoutes);


app.listen(3000,()=>{
    console.log('server running !');
})