import{ Provider, useDispatch, useSelector } from 'react-redux';
import store from './src/redux/store';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NativeWindStyleSheet } from "nativewind";
import ProfStack from './src/stacks-screens/ProfStack';
import AuthStack from './src/stacks-screens/AuthStack';
import { addToken } from './src/redux/AuthSlice';

NativeWindStyleSheet.setOutput({
  default: "native",
});
function App() {
  const {user} = useSelector(state => state.auth)
  const dispatch = useDispatch()
  useEffect(()=>{
    if(!user){
      dispatch(addToken())
    }
  },[user])
  return (
    // <NavigationContainer>
    //   {
    //     user? 
    //     user.is_teacher==1?
    //       <ProfStack/>:<AuthStack/>
    //       // data.user.is_teacher===1?
    //       // <AuthStack/>:null
    //     :<AuthStack />  
    //   }
    // </NavigationContainer>
     <NavigationContainer>
        <ProfStack/>
    </NavigationContainer>
  );
}

export default ()=>{
  return (
    <Provider store={store}>
      <App/>
     </Provider>
  )
}



