import {createSlice , createAsyncThunk} from "@reduxjs/toolkit";
import axios from "axios";

const baseUrl = "https://b734-41-249-65-61.ngrok-free.app"

export const getStudentResponses = createAsyncThunk("getStudentResponses" , async (data , thunkApi)=>{
    try {
        const {user_id , quiz_session_id} = data;
        const requestData = {
            user_id ,
            quiz_session_id
        }
        const config = {
            headers: {
              'If-Modified-Since': undefined,
            },
        };
        const response = await axios.post(`${baseUrl}/api/quizzes/quiz_passing/studentResponses` , requestData , config );
        return response.data;
    } catch (error) {
        console.error(error);
        return thunkApi.rejectWithValue(error.response.data || error.response || error);
    }
})

const initialState = {
    studentResponses : [],
    isLoading : false ,
    isError : false ,
    isSuccess : false ,
    message : ""
}

const studentResponsesSlice = createSlice({
    name : "studentResponses",
    initialState : initialState ,
    reducers : {
        reset : (state , action)=>{
            state.isLoading  = false 
            state.isError = false 
            state.isSuccess = false
            state.message = "" 
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getStudentResponses.pending, (state) => {
            state.isLoading = true;
            state.isError = false;
            state.isSuccess = false;
            state.message = "";
            state.studentResponses = []
        })
        builder.addCase(getStudentResponses.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isSuccess = true;
            state.message = "";
            state.studentResponses = action.payload.studentResponses ;
        })
        builder.addCase(getStudentResponses.rejected, (state, action) => {
            state.isLoading = false;
            state.isError = true;
            state.isSuccess = false;
            state.message = action.payload.message; 
            state.studentResponses = []
        })
    }
})

export const {reset} = studentResponsesSlice.actions ; 
export default studentResponsesSlice.reducer;