import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const baseUrl = "https://b734-41-249-65-61.ngrok-free.app";

// get quizzes
export const getQuestions_student = createAsyncThunk('getQuestions_student', async (data, thunkApi) => {
  try {
    const {quizId , isRandomQuestions , isRandomAnswers}= data ;
    const config = {
      headers: {
        'If-Modified-Since': undefined,
      },
    };
    const requestData = {
      quizId,
      isRandomQuestions,
      isRandomAnswers,
    };
    
    const response = await axios.post(`${baseUrl}/api/questions`,requestData, config );
    return response.data;
  } catch (error) {
    console.error(error);
    return thunkApi.rejectWithValue(error.response.data || error.response || error);
  }
});

  

const initialState = {
    questions: [],
    options : [],
    isError: false,
    isSuccess: false,
    isLoading: false,
    message: ""
}

const questionsSlice = createSlice({
    name: "questions",
    initialState: initialState,
    reducers: {
        reset: (state) => {
            state.isError = false;
            state.isLoading = false;
            state.isSuccess = false;
            state.message = "";
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getQuestions_student.pending, (state) => {
            state.isLoading = true;
            state.isError = false;
            state.isSuccess = false;
            state.message = "";
            state.questions = []
            state.options = []
        })
        builder.addCase(getQuestions_student.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isSuccess = true;
            state.message = "";
            state.questions = action.payload.questions
            state.options = action.payload.questionOptions
        })
        builder.addCase(getQuestions_student.rejected, (state, action) => {
            state.isLoading = false;
            state.isError = true;
            state.isSuccess = false;
            state.message = action.payload.message; 
            state.questions = []
            state.options = []
        })
    }
})


export const { reset } = questionsSlice.actions;

export default questionsSlice.reducer;