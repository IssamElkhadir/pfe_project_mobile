import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const baseUrl = "https://b734-41-249-65-61.ngrok-free.app"

// get quizzes
export const getQuizzes_student = createAsyncThunk('getQuizzes_student', async (userId, thunkApi) => {
  try {
    const config = {
      headers: {
        'If-Modified-Since': undefined,
      },
    };

    const response = await axios.post(`${baseUrl}/api/quizzes`, { userId: userId }, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return thunkApi.rejectWithValue(error.response.data || error.response || error);
  }
});
export const getQuizzes_student_for_reloading_quizzes = createAsyncThunk(
  "quizzes/getQuizzes_student_for_reloading_quizzes",
  async (userId, thunkApi) => {
    try {
      const config = {
        headers: {
          "If-Modified-Since": undefined,
        },
      };

      const response = await axios.post(
        `${baseUrl}/api/quizzes`,
        { userId: userId },
        config
      );
      return response.data;
    } catch (error) {
      console.error(error);
      return thunkApi.rejectWithValue(
        error.response.data || error.response || error
      );
    }
  }
);


const initialState = {
  quizzes: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: ""
}

const quizzesSlice = createSlice({
  name: "quizzes",
  initialState: initialState,
  reducers: {
    reset: (state) => {
      state.isError = false;
      state.isLoading = false;
      state.isSuccess = false;
      state.message = "";
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getQuizzes_student.pending, (state) => {
      state.isLoading = true;
      state.isError = false;
      state.isSuccess = false;
      state.message = "";
      state.quizzes = []
    })
    builder.addCase(getQuizzes_student.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.isSuccess = true;
      state.message = "";
      state.quizzes = action.payload.quizzes
    })
    builder.addCase(getQuizzes_student.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.isSuccess = false;
      state.message = action.payload.message;
      state.quizzes = []
    })

    //
    builder.addCase(getQuizzes_student_for_reloading_quizzes.fulfilled, (state, action) => {
      state.quizzes = action.payload.quizzes
    })
  }
})


export const { reset } = quizzesSlice.actions;

export default quizzesSlice.reducer;