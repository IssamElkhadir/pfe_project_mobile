import AsyncStorage from "@react-native-async-storage/async-storage";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const addToken = createAsyncThunk(
    'addtoken',
    async ()=>{
        //console.log('token')
       let result =  await AsyncStorage.getItem('data') ;
       result=JSON.parse(result)
       //console.log(result)
       return result;
    }
)
export const login = createAsyncThunk(
    'login',
    async ({name,password})=>{
        
       const res= await fetch("https://b17e-196-89-236-37.ngrok-free.app/api/auth/login",{
          method:"POST",
          headers: {
           'Content-Type': 'application/json'
         },
         body:JSON.stringify({
           "name":name,
           "password":password
         })
        })
        console.log(res);
        //return res.json();
        console.log(name);
    }
)
const AuthSlice=createSlice({
    name:"auth",
    initialState:{
        user:{
            "id": 60,
            "name": "Hassan Rachid",
            "name_ar": null,
            "email": null,
            "password": "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
            "remember_token": null,
            "created_by_id": 1,
            "updated_by_id": null,
            "deleted_by_id": null,
            "created_at": "2023-05-09T13:38:21.000Z",
            "updated_at": "2023-05-09T13:38:21.000Z",
            "deleted_at": null,
            "identity_number": "4",
            "first_name": "Hassan",
            "last_name": "Rachid",
            "first_name_ar": "حسن",
            "last_name_ar": "رشيد",
            "sexe": "M",
            "birth_date": null,
            "phone": null,
            "address": null,
            "address_ar": null,
            "postal_code": null,
            "school_id": 1,
            "city_id": null,
            "country_id": null,
            "nationality_id": null,
            "status": 1,
            "currency_id": null,
            "picture": null,
            "Remarks": null,
            "insurance_number": null,
            "insurance_name": null,
            "status_id": null,
            "is_admin": null,
            "is_teacher": 1,
            "is_tutor": null,
            "is_student": null,
            "student_number": null,
            "old_school_informations": null,
            "maternal_language_id": null,
            "birth_city_id": null,
            "birth_country_id": null,
            "religion_id": null,
            "prefered_days": null,
            "paiement_method_id": null,
            "selected_branche_id": null,
            "selected_level_id": null,
            "prospecting_source_id": null,
            "account_number": null,
            "paypal": null,
            "salary": null,
            "hourly_rate": null,
            "tax_number": null,
            "recruitment_date": null,
            "departure_date": null,
            "departure_reasons": null,
            "diploma_id": null,
            "banque_id": null,
            "function_id": null
        },
        token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjpbeyJpZCI6NjAsIm5hbWUiOiJIYXNzYW4gUmFjaGlkIiwibmFtZV9hciI6bnVsbCwiZW1haWwiOm51bGwsInBhc3N3b3JkIjoiJDJ5JDEwJDkySVhVTnBrak8wck9RNWJ5TWkuWWU0b0tvRWEzUm85bGxDLy5vZy9hdDIudWhlV0cvaWdpIiwicmVtZW1iZXJfdG9rZW4iOm51bGwsImNyZWF0ZWRfYnlfaWQiOjEsInVwZGF0ZWRfYnlfaWQiOm51bGwsImRlbGV0ZWRfYnlfaWQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIzLTA1LTA5VDEzOjM4OjIxLjAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAyMy0wNS0wOVQxMzozODoyMS4wMDBaIiwiZGVsZXRlZF9hdCI6bnVsbCwiaWRlbnRpdHlfbnVtYmVyIjoiNCIsImZpcnN0X25hbWUiOiJIYXNzYW4iLCJsYXN0X25hbWUiOiJSYWNoaWQiLCJmaXJzdF9uYW1lX2FyIjoi2K3Ys9mGIiwibGFzdF9uYW1lX2FyIjoi2LHYtNmK2K8iLCJzZXhlIjoiTSIsImJpcnRoX2RhdGUiOm51bGwsInBob25lIjpudWxsLCJhZGRyZXNzIjpudWxsLCJhZGRyZXNzX2FyIjpudWxsLCJwb3N0YWxfY29kZSI6bnVsbCwic2Nob29sX2lkIjoxLCJjaXR5X2lkIjpudWxsLCJjb3VudHJ5X2lkIjpudWxsLCJuYXRpb25hbGl0eV9pZCI6bnVsbCwic3RhdHVzIjoxLCJjdXJyZW5jeV9pZCI6bnVsbCwicGljdHVyZSI6bnVsbCwiUmVtYXJrcyI6bnVsbCwiaW5zdXJhbmNlX251bWJlciI6bnVsbCwiaW5zdXJhbmNlX25hbWUiOm51bGwsInN0YXR1c19pZCI6bnVsbCwiaXNfYWRtaW4iOm51bGwsImlzX3RlYWNoZXIiOjEsImlzX3R1dG9yIjpudWxsLCJpc19zdHVkZW50IjpudWxsLCJzdHVkZW50X251bWJlciI6bnVsbCwib2xkX3NjaG9vbF9pbmZvcm1hdGlvbnMiOm51bGwsIm1hdGVybmFsX2xhbmd1YWdlX2lkIjpudWxsLCJiaXJ0aF9jaXR5X2lkIjpudWxsLCJiaXJ0aF9jb3VudHJ5X2lkIjpudWxsLCJyZWxpZ2lvbl9pZCI6bnVsbCwicHJlZmVyZWRfZGF5cyI6bnVsbCwicGFpZW1lbnRfbWV0aG9kX2lkIjpudWxsLCJzZWxlY3RlZF9icmFuY2hlX2lkIjpudWxsLCJzZWxlY3RlZF9sZXZlbF9pZCI6bnVsbCwicHJvc3BlY3Rpbmdfc291cmNlX2lkIjpudWxsLCJhY2NvdW50X251bWJlciI6bnVsbCwicGF5cGFsIjpudWxsLCJzYWxhcnkiOm51bGwsImhvdXJseV9yYXRlIjpudWxsLCJ0YXhfbnVtYmVyIjpudWxsLCJyZWNydWl0bWVudF9kYXRlIjpudWxsLCJkZXBhcnR1cmVfZGF0ZSI6bnVsbCwiZGVwYXJ0dXJlX3JlYXNvbnMiOm51bGwsImRpcGxvbWFfaWQiOm51bGwsImJhbnF1ZV9pZCI6bnVsbCwiZnVuY3Rpb25faWQiOm51bGx9XSwiaWF0IjoxNjgzODA2NjI2fQ.dV28uO1oUHEBR2QF1kIGCOtSzSLgCSC_1N0_lpU7z7w",
        
        loading:false,
        error:null,
        user:null
    },
    reducers:{
        setData:(state,action)=>{
            state.user=action.payload.user;
            state.token=action.payload.access_token;
        },
        userLogout:(state,action)=>{
            state.user="";
            state.error="";
            state.loading=false;
            state.token="";
            AsyncStorage.clear();
        }
    },
    extraReducers:(builder)=>{
        //addToken
        builder.addCase(addToken.fulfilled,(state,action)=>{
            if(action.payload){
                state.user =action.payload.user;
                state.token=action.payload.token;
            }
        });
        login
        builder.addCase(login.pending,(state,action)=>{
            state.loading =true;
            state.user=null;
            state.token=null;
            state.error=null;
        });
        builder.addCase(login.fulfilled,async(state,action)=>{
            
            if(action.payload.error){
                state.error="username or password invalid";
            }
            else if(action.payload.name||action.payload.password){
                state.error="field required";
            }
            else if (action.payload.user)  {
                state.user =action.payload.user;
                state.token=action.payload.access_token;
                await AsyncStorage.setItem('data',JSON.stringify(action.payload))
            }
            state.loading =false;
        });
        builder.addCase(login.rejected,(state,action)=>{
            state.error =action.payload;
            state.loading =false;
        });
        
    }

    
})
export const {setData,userLogout}=AuthSlice.actions
export default AuthSlice.reducer