import { combineReducers, configureStore } from "@reduxjs/toolkit";
import AuthSlice from "./AuthSlice";
import QuizzesSlice from "./QuizzesSlice";
import QuestionsSlice from "./QuestionsSlice";
import studentResponsesSlice from "./studentResponses";
import studentResultsSlice from "./studentResults"; 

const reducers=combineReducers({
    auth:AuthSlice ,
    quizzes : QuizzesSlice ,
    questions : QuestionsSlice ,
    studentResponses : studentResponsesSlice ,
    studentResults : studentResultsSlice
})
const store=configureStore({
    reducer:reducers
})
export default store