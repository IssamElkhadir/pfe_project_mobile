import {createSlice , createAsyncThunk} from "@reduxjs/toolkit";
import axios from "axios";

const baseUrl = "https://b734-41-249-65-61.ngrok-free.app"

export const getStudentResults = createAsyncThunk("getStudentResults" , async (user_id , thunkApi)=>{
    try {
        const config = {
            headers: {
                'If-Modified-Since': undefined,
            },
        };
        const response = await axios.post(`${baseUrl}/api/student_results` , {user_id} , config );
        return response.data;
    } catch (error) {
        console.error(error);
        return thunkApi.rejectWithValue(error.response.data || error.response || error);
    }
})


export const get_student_results_for_reloading = createAsyncThunk("get_student_results_for_reloading" , async (user_id , thunkApi)=>{
    try {
        const config = {
            headers: {
                'If-Modified-Since': undefined,
            },
        };
        const response = await axios.post(`${baseUrl}/api/student_results` , {user_id} , config );
        return response.data;
    } catch (error) {
        console.error(error);
        return thunkApi.rejectWithValue(error.response.data || error.response || error);
    }
})

export const getCorrectedQuestions  = createAsyncThunk('get_corrected_questions' , async (data , thunkApi)=>{
    try {
        const {user_id , quiz_session_id} = data;
        const config = {
            headers: {
                'If-Modified-Since': undefined,
            },
        };
        const requestedData = {
            user_id ,
            quiz_session_id
        }
        const response = await axios.post(`${baseUrl}/api/student_results/get_corrected_questions` , requestedData , config );
        return response.data;
    } catch (error) {
        console.error(error);
        return thunkApi.rejectWithValue(error.response.data || error.response || error);
    }
})

const initialState = {
    studentResults : [],
    correctedQuestions : [],
    isLoading : false ,
    isError : false ,
    isSuccess : false ,
    message : ""
}

const studentResultsSlice = createSlice({
    name : "studentResults",
    initialState : initialState ,
    reducers : {
        reset : (state , action)=>{
            state.isLoading  = false 
            state.isError = false 
            state.isSuccess = false
            state.message = "" 
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getStudentResults.pending, (state) => {
            state.isLoading = true;
            state.isError = false;
            state.isSuccess = false;
            state.message = "";
            state.studentResults = []
        })
        builder.addCase(getStudentResults.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isSuccess = true;
            state.message = "";
            state.studentResults = action.payload.studentResults ;
        })
        builder.addCase(getStudentResults.rejected, (state, action) => {
            state.isLoading = false;
            state.isError = true;
            state.isSuccess = false;
            state.message = action.payload.message; 
            state.studentResults = []
        })
        //
        builder.addCase(get_student_results_for_reloading.fulfilled , (state , action)=>{
            state.studentResults = action.payload.studentResults;
        })
        // corrected questions
        builder.addCase(getCorrectedQuestions.pending, (state) => {
            state.isLoading = true;
            state.isError = false;
            state.isSuccess = false;
            state.message = "";
            state.correctedQuestions = []
        })
        builder.addCase(getCorrectedQuestions.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isSuccess = true;
            state.message = "";
            state.correctedQuestions = action.payload.correctedQuestions ;
        })
        builder.addCase(getCorrectedQuestions.rejected, (state, action) => {
            state.isLoading = false;
            state.isError = true;
            state.isSuccess = false;
            state.message = action.payload.message; 
            state.correctedQuestions = []
        })
    }
})

export const {reset} = studentResultsSlice.actions ; 
export default studentResultsSlice.reducer;