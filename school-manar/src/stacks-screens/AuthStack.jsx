import React, { useEffect, useState } from 'react';
import { NavigationContainer, PreventRemoveContext } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../pages/Login';
import { View ,Platform,Button} from 'react-native';
const Stack = createNativeStackNavigator();
export default function AuthStack(){
    return(
        
            <Stack.Navigator>
                <Stack.Screen 
                name="Login" 
                component={Login}
                options={{headerShown: false}}
                />
            </Stack.Navigator>
                

        
    );
}