import * as React from 'react';
import { View, Text ,StatusBar,TouchableOpacity,Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
  useDrawerProgress,
  useDrawerStatus,
} from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
import profile from '../../assets/login.png';
import CustomDrawerContent from '../customs/CustomDrawerContent';
import ExampleNavigation from '../navigations/ExampleNavigation';
import { ActivityIndicator } from 'react-native-paper';
import Loading from '../pages/Loading';
import HomeScreen from '../screens/HomeScreen';
import TimeTableScreen from '../screens/TimeTableScreen';
import MessageScreen from '../screens/MessageScreen';
import { useSelector } from 'react-redux';
import socket from '../utils/socket';

import { AcademicCapIcon } from 'react-native-heroicons/outline';
import QuizzesScreen from '../screens/QuizzesScreen';

function Article() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text className="text-red-500">Article Screen hhj</Text>
      </View>
    );
  }

const Drawer = createDrawerNavigator();
export default function ProfStack(props) {

    const [loading,setLoading]=React.useState(true);

    const {user} = useSelector(state => state.auth)

    React.useEffect(()=>{
        setTimeout(()=>{
            setLoading(false);
        },1000);
    },[loading]);
    React.useEffect(()=>{
        if(user){
          socket.emit("newUser", user.id);
        }
    },[user]);
    return ( loading?<Loading  />:
      <Drawer.Navigator
        screenOptions={options}
        useLegacyImplementation
        drawerContent={(props) => <CustomDrawerContent {...props} />}
      >
        <Drawer.Screen name="Dashboard" options={{
            drawerIcon: ({color}) => (
                  <Icon name="view-dashboard-outline" size={25} style={{marginRight: -20, color}} />
            ),
          }} component={HomeScreen } />
        <Drawer.Screen name="Time Table" options={{
            drawerIcon: ({color}) => (
                  <Icon name="clock-time-five-outline" size={25} style={{marginRight: -20, color}} />
            ),
          }} component={TimeTableScreen } />
        <Drawer.Screen name="Chatbox" options={{
            drawerIcon: ({color}) => (
                  <Icon name="newspaper" size={25} style={{marginRight: -20, color}} />
            ),
          }}   component={MessageScreen}/>
        <Drawer.Screen name="Notifications" options={{
            drawerIcon: ({color}) => (
                  <Icon name="bell" size={25} style={{marginRight: -20, color}} />
            ),
          }}   component={Article} />
        <Drawer.Screen name="Quizzes" options={{
            drawerIcon: ({color}) => (
                  <AcademicCapIcon size={25} color={color} />
            ),
          }}   component={QuizzesScreen} />
      </Drawer.Navigator>
    );
  }

const options={
    headerShown: false,
    headerStyle: {
      height: 60, 
    },
    drawerType: 'slide',
    drawerStyle: {
      width: 250,
      backgroundColor: "#0693e3",
    },
    overlayColor: null,
    drawerLabelStyle: {
      fontWeight: 'bold',
    },
    drawerActiveTintColor: "blue",
    drawerInactiveTintColor: "white",
    drawerLabelStyle: {
      marginLeft: 2 ,
      fontSize: 20,
    },
    drawerItemStyle: {backgroundColor: "tranparant"},
    sceneContainerStyle: {
      backgroundColor: "white",
      borderRadius: 50,
      color:"blue"
    },
  }