import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Header from "../components/header/Header";
import ExampleNavigation from "../navigations/ExampleNavigation";
import TimeTableNavigation from "../navigations/TimeTableNavigation";

const Stack = createNativeStackNavigator();
  function TimeTableScreen(props) {
    return (
      <Stack.Navigator
        screenOptions={{
          mode: 'card',
          headerShown: 'screen',
        }}
      >
        <Stack.Screen
          name="calendar"
          component={TimeTableNavigation}
          options={{
            headerShown: true,
            header: ({ navigation }) => (
              <Header title="Time Table"  navigation={navigation}  />
            ),
            cardStyle: { backgroundColor: '#FFFFFF' },
          }}
        />
      </Stack.Navigator>
    );
  }

export default TimeTableScreen