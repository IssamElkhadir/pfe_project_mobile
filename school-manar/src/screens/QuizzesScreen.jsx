import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Header from "../components/header/Header";
import QuizzesNavigation from "../navigations/QuizzesNavigation";
import QuizPassing from "../pages/quizzes/student/QuizPassing";
import StudentResultDetails from "../pages/quizzes/student/StudentResultDetails";

const Stack = createNativeStackNavigator();
  function QuizzesScreen(props) {
    return (
      <Stack.Navigator
        screenOptions={{
          mode: 'card',
          headerShown: 'screen',
        }}
      >
        <Stack.Screen
          name="Quizze"
          component={QuizzesNavigation}
          options={{
            headerShown: true,
            header: ({ navigation }) => (
              <Header title="Quizzes"  navigation={navigation} />
            ),
            cardStyle: { backgroundColor: '#FFFFFF' },
          }}
        />
        <Stack.Screen
          name="QuizPassing"
          component={QuizPassing}
          options={{
            headerShown: true,
            header: ({ navigation }) => (
              <Header title="QuizPassing"  navigation={navigation} />
            ),
            cardStyle: { backgroundColor: '#FFFFFF' },
          }}
        />
        <Stack.Screen
          name="StudentResultDetails"
          component={StudentResultDetails}
          options={{
            headerShown: true,
            header: ({ navigation }) => (
              <Header title="Results"  navigation={navigation} />
            ),
            cardStyle: { backgroundColor: '#FFFFFF' },
          }}
        />
      </Stack.Navigator>
    );
  }

export default QuizzesScreen