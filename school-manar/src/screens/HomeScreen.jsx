import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Header from "../components/header/Header";
import ExampleNavigation from "../navigations/ExampleNavigation";

const Stack = createNativeStackNavigator();

function HomeScreen(props) {
  return (
    <Stack.Navigator
      screenOptions={{
        mode: 'card',
        headerShown: 'screen',
      }}
    >
      <Stack.Screen
        name="Home"
        component={ExampleNavigation}
        options={{
          headerShown: true,
          header: ({ navigation }) => (
            <Header title="Home" navigation={navigation} />
          ),
          cardStyle: { backgroundColor: '#FFFFFF' },
        }}
      />
    </Stack.Navigator>
  );
}

export default HomeScreen