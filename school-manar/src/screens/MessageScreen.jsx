import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Header from "../components/header/Header";
import ExampleNavigation from "../navigations/ExampleNavigation";
import MessagesNavigation from "../navigations/MessagesNavigation";
import Chat from "../pages/messages/Chat";

const Stack = createNativeStackNavigator();
  function MessageScreen(props) {
    return (
      <Stack.Navigator
        screenOptions={{
          mode: 'card',
          headerShown: 'screen',
        }}
      >
        <Stack.Screen
          name="chatbox"
          component={MessagesNavigation}
          options={{
            headerShown: true,
            header: ({ navigation }) => (
              <Header title="Messages"  navigation={navigation}  />
            ),
            cardStyle: { backgroundColor: '#FFFFFF' },
          }}
        />
        <Stack.Screen
          name="Chat"
          component={Chat}
          options={{
            headerShown: true,
            header: ({ navigation }) => (
              <Header title="Chat"  navigation={navigation}  />
            ),
            cardStyle: { backgroundColor: '#FFFFFF' },
          }}
        />
      </Stack.Navigator>
    );
  }

export default MessageScreen;