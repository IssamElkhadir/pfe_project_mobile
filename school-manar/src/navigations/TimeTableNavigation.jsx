import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { View ,Text} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ProfCalendar from "../pages/timetable/Calendar";
import ProfTimeTable from "../pages/timetable/TimeTable";
import ProfAdvancement from "../pages/timetable/Advancement";

function Article() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View>
          <Text>azfzf</Text>
        </View>
        <Text>Article Screen hh</Text>
      </View>
    );
  }

const Tab = createMaterialTopTabNavigator();
export default function TimeTableNavigation() {

  return (
    <Tab.Navigator
      screenOptions={{
        // tabBarShowLabel:false,
        // title:false,
        // headerStyle: {
        //   height: 0, 
        // },
        tabBarLabelPosition:'bellow-icon',
        tabBarLabelStyle:{
          fontSize:10,
        },
        tabBarIndicatorStyle:{
          backgroundColor: '#309bd1',
          width:80,
          marginLeft:20,
        },
        tabBarActiveTintColor: '#309bd1',
        
        tabBarStyle: { backgroundColor: '#e6f5fc' ,borderBottomRightRadius:15,borderBottomLeftRadius:15},
      }}
    >
      <Tab.Screen name="Calendar" options={{
          tabBarIcon: ({color}) => (
                <Icon name="calendar-today" size={20} style={{marginRight: 1, color}} />
          ),
        }}  component={ProfCalendar}  />
      <Tab.Screen name="TimeTable" options={{
          tabBarIcon: ({color}) => (
                <Icon name="calendar-week" size={20} style={{marginRight: 1, color}} />
          ),
        }} component={ProfTimeTable}  />
      
      <Tab.Screen name="Advancement" options={{
          tabBarIcon: ({color}) => (
                <Icon name="calendar-multiple-check" size={20} style={{marginRight: 1, color}} />
          ),
        }}  component={ProfAdvancement} />
    </Tab.Navigator>
  );
}