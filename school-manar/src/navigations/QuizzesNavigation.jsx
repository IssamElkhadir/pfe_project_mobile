import React from 'react';
import { Animated, Dimensions, Image, PanResponder, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {PlusIcon , HomeIcon , MagnifyingGlassIcon , BellIcon , UserIcon, ArrowsUpDownIcon} from "react-native-heroicons/outline"
import { DataTable, Switch } from 'react-native-paper';
const optionsPerPage = [2, 3, 4];
import  StudentQuizzesResults from "../pages/quizzes/student/StudentQuizzesResults"

import { useRef } from 'react';
import Quizzes from '../pages/quizzes/student/Quizzes';
import { useState } from 'react';

const Tab = createBottomTabNavigator();

export default function QuizzesNavigation() {
  const tabOffsetValue = useRef(new Animated.Value(0)).current;
  return (
    <>
      <Tab.Navigator  screenOptions={{
        tabBarLabelStyle: {
          display: "none" 
        },
        tabBarIndicatorStyle:{
          width:0
        },
        style: {
          backgroundColor: 'white',
          position: 'absolute',
          bottom: 40,
          marginHorizontal: 20,
          height: 60,
          borderRadius: 10,
          shadowColor: '#000',
          shadowOpacity: 0.06,
          shadowOffset: {
            width: 10,
            height: 10
          },
          paddingHorizontal: 20,
        }
      }}>
        <Tab.Screen name={"Quizzes"} component={Quizzes} options={{
          tabBarIcon: ({ focused }) => (
            <View style={{
              position: 'absolute',
              top: 20
            }}>
              <HomeIcon
                size={20}
                color={focused ? '#3550dc' : 'gray'}
              />
            </View>
          ),
          tabBarIndicatorStyle:{
            width:0
          },
        }} ></Tab.Screen>
        {/* <Tab.Screen name={"ActionButton"} component={EmptyScreen} options={{
          tabBarIcon: ({ focused }) => (

            <TouchableOpacity>
              <View style={{
                width: 55,
                height: 55,
                backgroundColor: '#3550dc',
                borderRadius: 30,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: Platform.OS == "android" ? 50 : 30
              }}>
                <PlusIcon 
                    color={"white"}
                    size={30}
                />
              </View>
            </TouchableOpacity>
          )
        }}></Tab.Screen> */}

        <Tab.Screen name={"Student Results"} component={StudentQuizzesResults} options={{
          tabBarIcon: ({ focused }) => (
            <View style={{
              position: 'absolute',
              top: 20
            }}>
              <UserIcon
                size={20}
                color={focused ? '#3550dc' : 'gray'}
              />
            </View>
          )
        }} ></Tab.Screen>

      </Tab.Navigator>

      {/* <Animated.View style={{
        width: getWidth() - 20,
        height: 2,
        backgroundColor: '#3550dc',
        position: 'absolute',
        bottom: 98,
        left: 50,
        borderRadius: 20,
        transform: [
          { translateX: tabOffsetValue }
        ]
      }}>

      </Animated.View> */}
      </>
  );
}

function getWidth() {
  let width = Dimensions.get("window").width

  // Horizontal Padding = 20...
  width = width - 80

  // Total five Tabs...
  return width / 5
}

function EmptyScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    </View>
  );
}

function SettingsScreen() {
  const [options, setOptions] = useState([
    { id: '1', text: 'Option 1' },
    { id: '2', text: 'Option 2' },
    { id: '3', text: 'Option 3' },
    // Add more options as needed
  ]);

  const renderOptions = () => {
    return options.map((option) => (
      <QuestionOption key={option.id} option={option} />
    ));
  };

  const QuestionOption = ({ option }) => {
    const pan = useRef(new Animated.ValueXY()).current;

    const panResponder = useRef(
      PanResponder.create({
        onStartShouldSetPanResponder: () => true,
        onPanResponderMove: Animated.event(
          [null, { dx: pan.x, dy: pan.y }],
          { useNativeDriver: false }
        ),
        onPanResponderRelease: (_, gesture) => {
          const dropZoneIndex = Math.floor(
            (gesture.moveY - OPTION_CONTAINER_HEIGHT) / OPTION_HEIGHT
          );

          const updatedOptions = [...options];
          const currentIndex = updatedOptions.findIndex(
            (item) => item.id === option.id
          );

          updatedOptions.splice(currentIndex, 1);
          updatedOptions.splice(dropZoneIndex, 0, option);

          setOptions(updatedOptions);
          pan.setValue({ x: 0, y: 0 });
        },
      })
    ).current;

    return (
      <Animated.View
        style={[styles.optionContainer, pan.getLayout()]}
        {...panResponder.panHandlers}
      >
        <ArrowsUpDownIcon name="adjustments" size={20} color="#888" style={styles.dragIcon} />
        <Text style={styles.optionText}>{option.text}</Text>
        <ArrowsUpDownIcon name="trash" size={20} color="#888" style={styles.deleteIcon} />
      </Animated.View>
    );
  };

  return <View style={styles.container}>{renderOptions()}</View>;
}
const OPTION_CONTAINER_HEIGHT = 50;
const OPTION_HEIGHT = 40;


const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  optionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F8F8F8',
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginVertical: 5,
    borderRadius: 10,
    elevation: 2,
  },
  optionText: {
    flex: 1,
    fontSize: 16,
    marginLeft: 10,
  },
  dragIcon: {
    marginRight: 10,
  },
  deleteIcon: {
    marginLeft: 10,
  },
};