import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { View ,Text} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Messages from "../pages/messages/Messages";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import socket from "../utils/socket";


function Article() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View>
          <Text>azfzf</Text>
        </View>
        <Text>Article Screen hh</Text>
      </View>
    );
  }

const Tab = createMaterialTopTabNavigator();
export default function MessagesNavigation() {

  const {user}=useSelector(state=>state.auth);

  useEffect(()=>{
    if(user){
      socket.emit("newUser", user.id);
    }
},[user]);
  return (
    <Tab.Navigator
      screenOptions={{
        // tabBarShowLabel:false,
        // title:false,
        // headerStyle: {
        //   height: 0, 
        // },
        tabBarLabelPosition:'bellow-icon',
        tabBarLabelStyle:{
          fontSize:10,
          overflow:"hidden"
        },
        tabBarIndicatorStyle:{
          backgroundColor: '#309bd1',
          width:0
        },
        tabBarActiveTintColor: '#309bd1',
        
        tabBarStyle: { backgroundColor: '#e6f5fc' ,borderBottomRightRadius:15,borderBottomLeftRadius:15},
      }}
    >
      <Tab.Screen name="Chats" options={{
          tabBarIcon: ({color}) => (
                <Icon name="ios-chatbubbles-outline" size={20} style={{marginRight: 1, color}} />
          ),
        }}  component={Messages}  />
      <Tab.Screen name="Users" options={{
          tabBarIcon: ({color}) => (
                <Feather name="users" size={20} style={{marginRight: 1, color}} />
          ),
        }} component={Article}  />
      
    </Tab.Navigator>
  );
}