import React from "react";
import { Modal, Text, View, Pressable, Switch } from "react-native";

const DeleteModal = ({
  modalVisible,
  subject,
  handleDeleteModalVisibily,
  handleDeleteQuizSession ,
  handleDeleteModalVisibily_for_cancel
}) => {
  return (
    <Modal animationType="slide" transparent={true} visible={modalVisible}>
      <View className="flex-1 justify-center items-center bg-transparent">
        <View className="absolute top-0 bottom-0 left-0 right-0 bg-black opacity-50" />
        <View className="bg-white rounded-lg p-6 w-80 shadow-lg">
          <Text className="text-center text-lg font-bold mb-4">
            Confirm Delete
          </Text>
          <Text className="mb-4">
            Are you sure you want to delete this {subject}?
          </Text>
          <View className="flex-row justify-end">
            <Pressable className="bg-red-500 rounded-lg py-2 px-4 mr-2" onPress={handleDeleteQuizSession}>
              <Text className="text-white font-bold">Delete</Text>
            </Pressable>
            <Pressable
              className="bg-gray-500 rounded-lg py-2 px-4"
              onPress={handleDeleteModalVisibily_for_cancel}
            >
              <Text className="text-white font-bold">Cancel</Text>
            </Pressable>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default DeleteModal;
