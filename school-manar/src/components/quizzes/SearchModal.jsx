import React from 'react';
import { View, Text, Modal, Pressable, Switch } from 'react-native';

const SearchModal = ({ modalVisible, handleSearchOption, handleSearchModalVisibily, searchOption , subject }) => {
    return (
        <Modal animationType="slide" transparent={true} visible={modalVisible}>
            <View className="flex-1 justify-center items-center bg-transparent">
                <View className="absolute top-0 bottom-0 left-0 right-0 bg-black opacity-50" />
                {subject === "search_quiz" ? 
                <View className="bg-white rounded-lg p-6 w-80 shadow-lg">
                    <Text className="text-center text-lg font-bold mb-4">Select a search method</Text>
                    <Pressable
                        className="flex-row items-center mb-2"
                        onPress={() => handleSearchOption('by name')}
                    >
                        <Switch
                            className="transform scale-75"
                            value={searchOption === 'by name'}
                            onValueChange={() => handleSearchOption('by name')}
                            color="#3182CE"
                        />
                        <Text className="text-gray-700 font-bold ml-2">Search by quiz name</Text>
                    </Pressable>
                    <Pressable
                        className="flex-row items-center mb-2"
                        onPress={() => handleSearchOption('by number of questions')}
                    >
                        <Switch
                            className="transform scale-75"
                            value={searchOption === 'by number of questions'}
                            onValueChange={() => handleSearchOption('by number of questions')}
                            color="#3182CE"
                        />
                        <Text className="text-gray-700 font-bold ml-2">Search by number of questions</Text>
                    </Pressable>
                    <Pressable
                        className="flex-row items-center mb-2"
                        onPress={() => handleSearchOption('by duration')}
                    >
                        <Switch
                            className="transform scale-75"
                            value={searchOption === 'by duration'}
                            onValueChange={() => handleSearchOption('by duration')}
                            color="#3182CE"
                        />
                        <Text className="text-gray-700 font-bold ml-2">Search by quiz duration</Text>
                    </Pressable>
                    <Pressable
                        className="flex-row items-center mb-2"
                        onPress={() => handleSearchOption('by date')}
                    >
                        <Switch
                            className="transform scale-75"
                            value={searchOption === 'by date'}
                            onValueChange={() => handleSearchOption('by date')}
                            color="#3182CE"
                        />
                        <Text className="text-gray-700 font-bold ml-2">Search by date</Text>
                    </Pressable>
                    <Pressable
                        className="bg-gray-300 rounded-lg py-2 px-4 mt-2 self-end"
                        onPress={handleSearchModalVisibily}
                    >
                        <Text className="text-gray-700 font-bold">Close</Text>
                    </Pressable>
                </View>
                : <View className="bg-white rounded-lg p-6 w-80 shadow-lg">
                    <Text className="text-center text-lg font-bold mb-4">Select a search method</Text>
                    <Pressable
                        className="flex-row items-center mb-2"
                        onPress={() => handleSearchOption('by name')}
                    >
                        <Switch
                            className="transform scale-75"
                            value={searchOption === 'by name'}
                            onValueChange={() => handleSearchOption('by name')}
                            color="#3182CE"
                        />
                        <Text className="text-gray-700 font-bold ml-2">Search by quiz name </Text>
                    </Pressable>
                    <Pressable
                        className="flex-row items-center mb-2"
                        onPress={() => handleSearchOption('by score')}
                    >
                        <Switch
                            className="transform scale-75"
                            value={searchOption === 'by score'}
                            onValueChange={() => handleSearchOption('by score')}
                            color="#3182CE"
                        />
                        <Text className="text-gray-700 font-bold ml-2">Search by score </Text>
                    </Pressable>
                    <Pressable
                        className="flex-row items-center mb-2"
                        onPress={() => handleSearchOption('by date')}
                    >
                        <Switch
                            className="transform scale-75"
                            value={searchOption === 'by date'}
                            onValueChange={() => handleSearchOption('by date')}
                            color="#3182CE"
                        />
                        <Text className="text-gray-700 font-bold ml-2">Search by date </Text>
                    </Pressable>
                    <Pressable
                        className="bg-gray-300 rounded-lg py-2 px-4 mt-2 self-end"
                        onPress={handleSearchModalVisibily}
                    >
                        <Text className="text-gray-700 font-bold">Close</Text>
                    </Pressable>
                </View>
                }
            </View>
        </Modal>
    );
};

export default SearchModal;
