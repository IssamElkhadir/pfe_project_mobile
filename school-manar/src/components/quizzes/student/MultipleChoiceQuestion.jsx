import { View, Text  , TouchableOpacity , ScrollView, Image} from 'react-native'
import React, { useEffect, useState } from 'react'

const MultipleChoiceQuestion = ({question_options ,user_answer , handleUserAnswer , quiz_session_id , question_id , question_type }) => {
    
    if( user_answer !== null){
        if(user_answer.length > 1){
            user_answer  = user_answer.map((item)=> Number(item))
        }
    }
    console.log(user_answer)
    const is_MultipleAnswers = (id) => {
        if (question_type === "multiple_choice") {
            const correctOptions = question_options[id].filter(option => option.is_correct === 1);
            if (correctOptions.length > 1) {
                return true;
            } else {
                return false;
            }
        }
    }
    // select one option
    const [selectedOption, setSelectedOption] = useState(user_answer ? Number(user_answer) : null);
    useEffect(() => {
        handleUserAnswer(quiz_session_id, question_id, selectedOption, "multiple_choice_oneAnswer")
    }, [selectedOption])
    const handleSelectOption = (option_id) => {
        setSelectedOption(option_id);
    };
    // select multiple options
    const [selectedOptions, setSelectedOptions] = useState(user_answer ? user_answer : []);
    useEffect(() => {
        handleUserAnswer(quiz_session_id, question_id, selectedOptions, "multiple_choice_multipleAnswer")
    }, [selectedOptions])
    const handleSelectOptions = (option_id) => {
        if (selectedOptions.includes(option_id)) {
            // Remove the option if it is already selected
            setSelectedOptions(selectedOptions.filter((id) => id !== option_id));
        } else {
            // Add the option if it is not selected
            setSelectedOptions([...selectedOptions, option_id]);
        }
    };
    return (
        <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
            <View className="grid grid-cols-1 md:grid-cols-3 gap-4">
                {question_options &&
                    is_MultipleAnswers(question_id) === false ? (
                    question_options[question_id].map((option, index) => (
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <TouchableOpacity
                                key={option.id}
                                onPress={() => handleSelectOption(option.id)}
                                className={`flex flex-row items-center min-w-full justify-start rounded-full p-3 ${selectedOption === option.id ? 'bg-blue-500' : 'bg-gray-200'
                                    }`}
                            >
                                <View className="w-8 h-8 mr-2 rounded-full flex items-center justify-center">
                                    <Text
                                        className={`text-gray-800 ${selectedOption === option.id ? 'text-white' : ''}`}
                                    >
                                        {String.fromCharCode(65 + index) + "  -"}
                                    </Text>
                                </View>
                                <Text>{option.option_text}</Text>
                                {option.option_picture && (
                                    <View className="relative w-8 h-8 ml-2">
                                        <Image
                                            source={{ uri: option.option_picture }}
                                            className="absolute inset-0 object-cover rounded-full w-full h-full"
                                        />
                                    </View>
                                )}
                            </TouchableOpacity>
                        </ScrollView>
                    ))
                ) : question_options[question_id].map((option, index) => (
                    <TouchableOpacity
                        key={option.id}
                        onPress={() => handleSelectOptions(option.id)}
                        className={`flex flex-row items-center justify-start rounded-full p-3 ${selectedOptions.includes(
                            option.id
                        )
                            ? 'bg-blue-500'
                            : 'bg-gray-200'
                            }`}
                    >
                        <View className="w-8 h-8 mr-2 rounded-full flex items-center justify-center">
                            <Text
                                className={`text-gray-800 ${selectedOptions.includes(option.id) ? 'text-white' : ''
                                    }`}
                            >
                                {String.fromCharCode(65 + index) + "  -"}
                            </Text>
                        </View>
                        <Text>{option.option_text}</Text>
                        {option.option_picture && (
                            <View className="relative w-8 h-8 ml-2">
                                <Image
                                    source={{ uri: option.option_picture }}
                                    className="absolute inset-0 object-cover rounded-full w-full h-full"
                                />
                            </View>
                        )}
                    </TouchableOpacity>
                ))}
            </View>
        </ScrollView>
    )
}

export default MultipleChoiceQuestion