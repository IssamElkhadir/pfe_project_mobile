import React, { useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  Dimensions
} from "react-native";

import { DragSortableView } from "react-native-drag-sort";

const { width } = Dimensions.get("window");
const parentWidth = width;
const childrenWidth = 350;
const childrenHeight = 76;
const marginChildrenTop = 0;
const marginChildrenBottom = 0;
const marginChildrenLeft = 0;
const marginChildrenRight = 0;


const ReorderQuestion = ({ question_options, setScrollEnabled , user_answer , handleUserAnswer , quiz_session_id , question_id , question_type }) => {
  const [data, setData] = useState(question_options);
  
  if(user_answer !== null){
    user_answer = user_answer.map((item)=> Number(item));
  }
  useEffect(() => {
    if(user_answer){
      const sortedData = [...data].sort((a, b) => {
        const orderA = user_answer.indexOf(a.option_order);
        const orderB = user_answer.indexOf(b.option_order);
        return orderA - orderB;
      });
      setData(sortedData);
    }
  }, []);
  const renderItem = (item, index) => {
    const itemNumber = index + 1;

    return (
      <View  key={item.id} style={styles.item} className="bg-gray-200 rounded-full">
        <View className="w-full h-full mt-2.5 pl-2.5">
          <View className="flex flex-row space-x-2 items-center">
            <View className="w-8 h-8 mr-2 rounded-full flex items-center justify-center">
              <Text>{itemNumber}  -</Text>
            </View>
            <Text>{item.option_text}</Text>
            {item.option_picture && <View className="relative w-8 h-8 ml-2">
              <Image
                source={{ uri: item.option_picture }}
                className="absolute inset-0 object-cover rounded-full w-full h-full"
              />
            </View>}
          </View>
        </View>
      </View>
    );
  };


  const handleDragStart = (startIndex, endIndex) => {
    setScrollEnabled(false);
  };

  const handleDragEnd = (startIndex, endIndex ) => {
    setScrollEnabled(true);
    
  };

  const onDataChange = (data) => {
    const userRank = data.map((item)=>item.option_order)
    handleUserAnswer(quiz_session_id , question_id , userRank , question_type)
  }

  return (
    <View

      style={styles.container}
    >
      <View style={styles.sortContainer}>
        <DragSortableView
          dataSource={data}
          parentWidth={parentWidth}
          childrenWidth={childrenWidth}
          childrenHeight={childrenHeight}
          marginChildrenTop={marginChildrenTop}
          marginChildrenBottom={marginChildrenBottom}
          marginChildrenLeft={marginChildrenLeft}
          marginChildrenRight={marginChildrenRight}
          onDragStart={handleDragStart}
          onDragEnd={handleDragEnd}
          onDataChange={onDataChange}
          keyExtractor={(item, index) => item.id.toString()}
          renderItem={(item, index) => renderItem(item, index)}
          style={styles.sortView}
        />
      </View>
    </View>
  );
}

export default ReorderQuestion;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sortContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  sortView: {
    width: '100%',
  },
  txt: {
    lineHeight: 24,
    padding: 5,
  },
  sort: {
    marginLeft: 20,
  },
  item: {
    width: childrenWidth,
    height: childrenHeight - 20,
  }
});