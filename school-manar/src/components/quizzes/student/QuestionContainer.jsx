import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    TextInput,
    Button,
    StyleSheet,
    ActivityIndicator,
    Switch,
    PanResponder,
    Animated,
} from "react-native";

import React, { useState, useRef, useEffect } from "react";
import QuestionMultimediaContainer from "./QuestionMultimediaContainer";
import QuestionExplanation from "./QuestionExplanation";
import FillInTheBlankQuestion from "./FillInTheBlankQuestion";
import OpenEndedQuestion from "./OpenEndedQuestion";
import MultipleChoiceQuestion from "./MultipleChoiceQuestion";
import ReorderQuestion from "./ReorderQuestion";

const QuestionContainer = ({
    quiz_session_id,
    question_text,
    question_type,
    multimedia_type,
    multimedia,
    question_explanation,
    question_options,
    question_id,
    handleUserAnswer,
    user_answer,
}) => {
    // disable scrolling when dragging
    const scrollViewRef = useRef(null);
    const [scrollEnabled, setScrollEnabled] = useState(true);

    return (
        <ScrollView
            ref={scrollViewRef}
            scrollEnabled={scrollEnabled}
            vertical={true}
            showsVerticalScrollIndicator={false}
            key={question_id}
            className="w-full mt-5 h-[480px]"
        >
            <View className="bg-white dark:bg-gray-800 shadow rounded-lg p-4">
                <View className="flex flex-row justify-center w-full mb-7">
                    <QuestionMultimediaContainer
                        multimedia={multimedia}
                        question_text={question_text}
                        multimedia_type={multimedia_type}
                        question_id={question_id}
                    />
                </View>
                <View className="text-gray-700 dark:text-gray-500 font-medium mb-4">
                    <Text>{question_text}</Text>
                </View>

                <View className="w-full">
                    {question_type === "fill_in_the_blank" ? (
                        <FillInTheBlankQuestion
                            user_answer={user_answer}
                            handleUserAnswer={handleUserAnswer}
                            question_id={question_id}
                            quiz_session_id={quiz_session_id}
                            question_type={question_type}
                        />
                    ) : question_type === "open-ended" ? (
                        <OpenEndedQuestion
                            user_answer={user_answer}
                            handleUserAnswer={handleUserAnswer}
                            question_id={question_id}
                            quiz_session_id={quiz_session_id}
                            question_type={question_type}
                        />
                    ) : question_type === "multiple_choice" ? (
                        <MultipleChoiceQuestion
                            question_options={question_options}
                            user_answer={user_answer}
                            handleUserAnswer={handleUserAnswer}
                            question_id={question_id}
                            quiz_session_id={quiz_session_id}
                            question_type={question_type}
                        />
                    ) : (
                        question_type === "reorder" && (
                            <ReorderQuestion
                                question_options={question_options[question_id]}
                                setScrollEnabled={setScrollEnabled}
                                user_answer={user_answer}
                                handleUserAnswer={handleUserAnswer}
                                question_id={question_id}
                                quiz_session_id={quiz_session_id}
                                question_type={question_type}
                            />
                        )
                    )}
                </View>

                <QuestionExplanation question_explanation={question_explanation} />
            </View>
        </ScrollView>
    );
};

export default QuestionContainer;
