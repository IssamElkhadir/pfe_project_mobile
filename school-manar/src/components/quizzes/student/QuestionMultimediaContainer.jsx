import { View, Text, ActivityIndicator, Image, Button, TouchableOpacity } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { Video, ResizeMode } from 'expo-av';
import Slider from '@react-native-community/slider';
import {PauseCircleIcon, PauseIcon, PlayCircleIcon, PlayIcon, PlayPauseIcon} from "react-native-heroicons/outline"
import { Audio } from 'expo-av';
const QuestionMultimediaContainer = ({ multimedia, multimedia_type, question_text }) => {
    // video
    const video = useRef(null);
    const [isLoading, setIsLoading] = useState(true);
    const [status, setStatus] = useState({});

    const handleVideoLoad = () => {
        setIsLoading(false);
    };
    const handlePlaybackStatusUpdate = (newStatus) => {
        setStatus(newStatus);
    };

    // audio
    const [isPlaying, setIsPlaying] = useState(false);
    const [position, setPosition] = useState(0);
    const [duration, setDuration] = useState(0);
    const [seekPosition, setSeekPosition] = useState(0);
    const sound = useRef(null);

    useEffect(() => {
        if (sound.current) {
            sound.current.setOnPlaybackStatusUpdate((status) => {
                if (status.isLoaded) {
                    setIsPlaying(status.isPlaying);
                    setPosition(status.positionMillis);
                    setDuration(status.durationMillis);
                    setSeekPosition(status.positionMillis);
                }
            });
        }
    }, [sound.current]);

    useEffect(() => {
        const interval = setInterval(() => {
            if (isPlaying && seekPosition < duration) {
                setSeekPosition(seekPosition + 1000);
            }
        }, 1000);

        return () => {
            clearInterval(interval);
        };
    }, [isPlaying, seekPosition, duration]);


    const loadSound = async () => {
        try {
            console.log('Loading Sound');
            const { sound: audioSound } = await Audio.Sound.createAsync(require('../../../../assets/test.mp3'));
            sound.current = audioSound;
            console.log('Playing Sound');
            await audioSound.playAsync();
            setIsPlaying(true);
        } catch (error) {
            console.log('Error playing audio: ', error);
        }
    };

    const stopSound = async () => {
        if (sound.current) {
            await sound.current.stopAsync();
            setIsPlaying(false);
            setPosition(0);
            setSeekPosition(0);
        }
    };

    const handlePlay = async () => {
        try {
            if (sound.current) {
                if (isPlaying) {
                    await sound.current.pauseAsync();
                    setIsPlaying(false);
                } else {
                    await sound.current.playAsync();
                    setIsPlaying(true);
                }
            } else {
                await loadSound();
            }
        } catch (error) {
            console.log('Error playing audio: ', error);
        }
    };

    const handleSeek = async (value) => {
        if (sound.current) {
            await sound.current.setPositionAsync(value);
            setSeekPosition(value);
        }
    };

    const formatTime = (time) => {
        const minutes = Math.floor(time / 60000);
        const seconds = Math.floor((time % 60000) / 1000);
        return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    };

    useEffect(() => {
        return () => {
            stopSound();
        };
    }, []);
    return (
        <>
            {multimedia && multimedia_type === "image" ? (
                <View className="relative w-[100px] h-[100px]">
                    <Image
                        source={{
                            uri: `https://media.istockphoto.com/id/1334705699/vector/morocco-african-country-flag.jpg?s=612x612&w=0&k=20&c=bGd03v1OWUllEI-Vvp30L57eR59nUVCqGwHT7Qv17Uc=`
                        }}
                        className="absolute inset-0 object-cover w-full h-full rounded"
                    />
                </View>
            ) : (
                multimedia && multimedia_type === "video" ?
                    <View className="flex-1 justify-center items-center">
                        <View className="relative w-[300px] h-[200px] rounded">
                            {isLoading && (
                                <View className="absolute top-0 left-0 right-0 bottom-0 bg-gray-900 ">
                                    <ActivityIndicator color="white" size="large" style={{ flex: 1 }} />
                                </View>
                            )}

                            <Video
                                ref={video}
                                source={{ uri: "https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" }}
                                useNativeControls
                                resizeMode={ResizeMode.CONTAIN}
                                isLooping
                                onReadyForDisplay={handleVideoLoad}
                                onPlaybackStatusUpdate={handlePlaybackStatusUpdate}
                                className="w-full h-full rounded"
                            />
                        </View>

                        <View>
                            <Button
                                title={status.isPlaying ? 'Pause' : 'Play'}
                                onPress={() => (status.isPlaying ? video.current.pauseAsync() : video.current.playAsync())}
                            />
                        </View>
                    </View>

                    : (multimedia && multimedia_type === "voice") &&
                    <View className="flex flex-row items-center bg-blue-400 p-2 px-8 rounded-full">
                        <TouchableOpacity onPress={handlePlay} className="mb-2 pr-4 mt-1">
                            {isPlaying ?   <PauseCircleIcon size={50} color={"white"}/> : <PlayCircleIcon size={50} color={"white"}/>}
                        </TouchableOpacity>
                        <View className="mr-6">
                            <Slider
                                value={seekPosition}
                                minimumValue={0}
                                maximumValue={duration}
                                step={1000}
                                onSlidingComplete={handleSeek}
                            />
                            <Text className="text-white">{`${formatTime(seekPosition)} / ${formatTime(duration)}`}</Text>
                        </View>
                    </View>
            )}
        </>
    )
}

export default QuestionMultimediaContainer