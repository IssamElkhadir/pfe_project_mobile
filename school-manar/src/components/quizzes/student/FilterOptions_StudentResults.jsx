import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import React from 'react'

const FilterOptions_StudentResults = ({handleActiveFilterOption , activeFilterOption}) => {
    const filter_options = [
        {text : "Newest" , id : 1},
        {text : "Oldest" , id : 2},
        {text : "Last 3 days" , id : 3},
        {text : "Last week" , id : 4},
        {text : "Last month" , id : 5},
        {text : "Last 3 months" , id : 6},
        {text : "Last year" , id : 7},
        {text : "Highest score" , id : 8},
        {text : "Lowest score" , id : 9},
    ]
    return (
        <View className="w-full flex-row">
            <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{ paddingHorizontal: 10 }}
            >
                {filter_options.map((option) => (
                    
                    <TouchableOpacity onPress={()=> handleActiveFilterOption(option.text)} key={option.id} className={`mr-3 px-2.5 py-2 ${activeFilterOption === option.text ? "bg-slate-300" : "bg-slate-100"} rounded-full`}>
                        <Text className="text-[13px]">{option.text}</Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>
        </View>
    )
}

export default FilterOptions_StudentResults