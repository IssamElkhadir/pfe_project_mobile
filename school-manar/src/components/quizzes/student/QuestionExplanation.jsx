import { View, Text } from 'react-native'
import React from 'react'

const QuestionExplanation = ({ question_explanation }) => {
    return (
        <>
            {question_explanation && (
                <View className="text-gray-700 dark:text-gray-500 font-medium mb-4 mt-10">
                    <Text className="font-bold text-gray-900 pl-1">Question explanation :</Text>
                        <Text className="text-xs pl-5 mt-2">{question_explanation}</Text>
                </View>
            )}
        </>
    )
}

export default QuestionExplanation