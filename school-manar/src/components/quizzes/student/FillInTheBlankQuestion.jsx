import { View, Text, TextInput } from 'react-native'
import React, { useEffect, useState } from 'react'

const FillInTheBlankQuestion = ({user_answer , handleUserAnswer , quiz_session_id , question_id , question_type}) => {
    const [fillInTheBlankAnswer, setFillInTheBlankAnswer] = useState(user_answer ? user_answer : '');
    useEffect(()=>{
        handleUserAnswer(quiz_session_id, question_id, fillInTheBlankAnswer, question_type)
    } , [fillInTheBlankAnswer])
    const onChangeFillInTheBlankAnswer = (text) => {
        setFillInTheBlankAnswer(text);
    };
    return (
        <TextInput
            placeholder="Type your answer here"
            caretHidden={false}
            selectionColor="#000"
            activeUnderlineColor="transparent"
            underlineColor="transparent"
            keyboardType="default"
            value={fillInTheBlankAnswer}
            onChangeText={onChangeFillInTheBlankAnswer}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        />
    )
}

export default FillInTheBlankQuestion