import { View, Text, TextInput } from 'react-native'
import React, { useEffect, useState } from 'react'

const OpenEndedQuestion = ({user_answer , handleUserAnswer , quiz_session_id , question_id , question_type}) => {
    const [openEndedAnswer, setOpenEndedAnswer] = useState(user_answer ? user_answer : '');
    useEffect(()=>{
        handleUserAnswer(quiz_session_id, question_id, openEndedAnswer, question_type)
    } , [openEndedAnswer])
    const onChangeOpenEndedAnswer = (text) => {
        setOpenEndedAnswer(text);
    };
    return (
        <TextInput
            multiline={true}
            numberOfLines={4}
            textAlignVertical="top"
            value={openEndedAnswer}
            onChangeText={onChangeOpenEndedAnswer}
            placeholder="Type your answer here "
            className="p-2.5 w-full h-[90px] text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        />
    )
}

export default OpenEndedQuestion