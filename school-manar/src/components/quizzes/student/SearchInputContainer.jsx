import { View, Text } from 'react-native'
import React from 'react'
import { TextInput } from 'react-native-paper'
import { AdjustmentsVerticalIcon, MagnifyingGlassIcon } from 'react-native-heroicons/outline'

const SearchInputContainer = ({ handleSearchModalVisibily, searchText, handleSearch }) => {
    return (
        <View className="flex-row items-center space-x-2 bg-white  rounded-lg mt-4 px-4 mx-3">
            <View className="flex-1 flex-row items-center space-x-2 ">
                <MagnifyingGlassIcon />
                <TextInput
                    placeholder="search"
                    caretHidden={false}
                    value={searchText}
                    onChangeText={handleSearch}
                    selectionColor="#000"
                    activeUnderlineColor="transparent"
                    underlineColor="transparent"
                    keyboardType="default"
                    className="w-[80%] focus:underline-offset-0 focus:outline-0 focus:border-none bg-transparent border-none text-gray-900"
                />
            </View>
            <AdjustmentsVerticalIcon onPress={handleSearchModalVisibily} />
        </View>
    )
}

export default SearchInputContainer