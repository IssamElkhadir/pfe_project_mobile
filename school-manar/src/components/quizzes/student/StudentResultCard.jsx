import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { CheckIcon, InformationCircleIcon, XMarkIcon } from 'react-native-heroicons/outline'
import { useNavigation } from '@react-navigation/native'

const StudentResultCard = ({result , user_id}) => {
    let endAt = new Date(result.end_at);
    const navigation = useNavigation();
    const studentRemark = (score) => {
        let msg;
      
        switch (true) {
          case score < 10:
            msg = "Unfortunately, you did not perform well on this quiz.";
            break;
          case score < 20:
            msg = "Your score indicates that there is much room for improvement.";
            break;
          case score < 30:
            msg = "Your score indicates that there is significant room for improvement.";
            break;
          case score < 40:
            msg = "Your score indicates that there is room for improvement.";
            break;
          case score < 50:
            msg = "Your score indicates that there is some room for improvement.";
            break;
          case score < 60:
            msg = "Congratulations, you passed the quiz! However, there is still room for improvement. Please review the material and strive for a higher score next time.";
            break;
          case score < 70:
            msg = "Congratulations, you passed the quiz with a good score! Keep up the good work and continue to review the material.";
            break;
          case score < 80:
            msg = "Congratulations, you passed the quiz with a great score! Well done and keep up the hard work.";
            break;
          case score < 90:
            msg = "Congratulations, you passed the quiz with an excellent score! You have a solid understanding of the material.";
            break;
          case score < 100:
            msg = "Congratulations, you passed the quiz with an outstanding score! You have demonstrated a deep understanding of the material.";
            break;
          case score === 100:
            msg = "Congratulations, you aced the quiz! You have a thorough understanding of the material. Keep up the great work!";
            break;
          default:
            msg = "";
            break;
        }
      
        return msg;
    };

    const goToStudentResultsDetailsPage = () =>{
      navigation.navigate('StudentResultDetails' ,{result , user_id})
    }
    return (
        <TouchableOpacity
          onPress={goToStudentResultsDetailsPage}
            key={result.id}
            className={`flex flex-row items-center p-4 bg-white rounded-lg shadow-md relative mb-2 mt-3
                    `}
        >
            <TouchableOpacity
                className="absolute top-1 right-1"
            >
                <InformationCircleIcon size={30} color={"grey"} />
            </TouchableOpacity>
            <View className={`w-[100px] h-[100px] overflow-hidden rounded-full border justify-center items-center ${result.score <= 50 ? "border-red-600" : "border-green-600"}`}>
                <Text className={`text-2xl ${result.score <= 50 ? "text-red-600" : "text-green-600"}`}>{result.score} %</Text>
            </View>

            <View className="flex flex-col ml-4">
                <Text className="text-lg font-bold text-blue-600">{result.quiz_name}</Text>
                <Text className="text-gray-500">number of questions : {result.total_questions_count}</Text>
                <View className="flex flex-row items-center space-x-2 mt-1">
                    <CheckIcon color="gray" size={18} />
                    <Text className="text-gray-500">{result.correct_questions_count} correct question</Text>
                </View>
                <View className="flex flex-row items-center space-x-2 mt-1">
                    <XMarkIcon color="gray" size={18} />
                    <Text className="text-gray-500">{result.incorrect_questions_count} incorrect question</Text>
                </View>
                <View className="flex flex-row items-center space-x-2 mt-1">
                    <Text className="text-gray-500">
                        end at : {endAt.toLocaleString()}
                    </Text>
                </View>
                <View className="flex flex-row items-center space-x-2 mt-2">
                    <View className="w-[80%] ">
                        <Text className="text-gray-500">remark :</Text>
                        <Text className="text-xs ml-4 ">{studentRemark(result.score)}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default StudentResultCard;