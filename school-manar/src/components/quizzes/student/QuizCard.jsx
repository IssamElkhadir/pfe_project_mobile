import { View, Text, TouchableOpacity, Image } from "react-native";
import React from "react";
import {
    ClockIcon,
    ListBulletIcon,
    TrashIcon,
} from "react-native-heroicons/outline";
import { useNavigation } from "@react-navigation/native";

const QuizCard = ({
    user_id ,
    id,
    quiz , 
    quizName,
    quizDuration,
    quizsessionDuration,
    quizsession_started_at,
    quizsession_ended_at,
    quizNbQuestions,
    is_continue_quiz,
    start_at,
    end_at,
    activeQuiz,
    handleActiveQuiz,
    handleDeleteModalVisibily,
}) => {
    const navigation = useNavigation();
    let startAt = new Date(start_at);
    let endAt = new Date(end_at);
    let quizsession_startedAt = new Date(quizsession_started_at);
    let quizsession_endedAt = quizsession_ended_at
        ? new Date(quizsession_ended_at)
        : null;
    const formatDuration = (duration) => {
        const hours = Math.floor(duration / 3600);
        const minutes = Math.floor((duration % 3600) / 60);
        const seconds = duration % 60;

        const formattedHours = hours.toString().padStart(2, "0");
        const formattedMinutes = minutes.toString().padStart(2, "0");
        const formattedSeconds = seconds.toString().padStart(2, "0");

        return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
    };
    // 
    const hanldeContinueQuiz = () => {
        navigation.navigate("QuizPassing", { quiz: quiz  , status : 'continue_quiz' , user_id});
    }
    return (
        <TouchableOpacity
            key={id}
            onPress={() => {
                if (is_continue_quiz === false) {
                    handleActiveQuiz(id);
                }
            }}
            className={`flex flex-row items-center p-4 bg-white rounded-lg shadow-md relative mb-2 ${activeQuiz === id ? "border border-blue-500" : ""
                }`}
        >
            {is_continue_quiz === true && (
                <TouchableOpacity
                    onPress={()=> handleDeleteModalVisibily(id)}
                    className="absolute top-1 right-1"
                >
                    <TrashIcon size={23} color={"grey"} />
                </TouchableOpacity>
            )}
            <Image
                className="w-16 h-16 overflow-hidden"
                resizeMode="cover"
                source={require("../../../../assets/quiz_pic.png")}
            />
            <View className="flex flex-col ml-4">
                <Text className="text-lg font-bold text-blue-600">{quizName}</Text>
                <View className="flex flex-row items-center space-x-2 mt-1">
                    <ListBulletIcon color="gray" size={15} />
                    <Text className="text-gray-500">{quizNbQuestions} Questions</Text>
                </View>
                {is_continue_quiz === false ? (
                    <View className="flex flex-row items-center space-x-2">
                        <ClockIcon color="gray" size={15} />
                        <Text className="text-gray-500">{quizDuration} minutes</Text>
                    </View>
                ) : (
                    <View className="flex flex-row items-center space-x-2">
                        <ClockIcon color="gray" size={15} />
                        <Text className="text-gray-500">
                            {formatDuration(quizsessionDuration)} remaining time
                        </Text>
                    </View>
                )}
                {is_continue_quiz === true &&  (
                    <View className="flex flex-row items-center space-x-2">
                        <Text className="text-gray-500">
                            started at : {quizsession_startedAt.toLocaleString()}
                        </Text>
                    </View>
                )}
                {is_continue_quiz === false ? (
                    <View className="flex flex-row items-center space-x-2">
                        <Text className="text-gray-500">
                            end at : {endAt.toLocaleString()}
                        </Text>
                    </View>
                ) : (
                    quizsession_endedAt !== null && <View className="flex flex-row items-center space-x-2">
                        <Text className="text-gray-500">
                            ended at : {quizsession_endedAt.toLocaleString()}
                        </Text>
                    </View>
                )}

                {is_continue_quiz === true && (
                    <TouchableOpacity
                        onPress={hanldeContinueQuiz}
                        className="bg-gray-900 mt-3 text-white rounded-lg py-3 px-20 mr-5 mb-1"
                    >
                        <Text className="text-white text-base font-bold text-center">
                            Continue quiz
                        </Text>
                    </TouchableOpacity>
                )}
            </View>
        </TouchableOpacity>
    );
};

export default QuizCard;
