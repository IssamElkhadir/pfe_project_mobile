import { Text, TouchableOpacity, View } from "react-native";
import Icon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { SafeAreaView } from 'react-native-safe-area-context';
const Header=({navigation ,title})=>{
    return(
        <View className= "bg-[#e6f5fc]">
            <View style={{
                flexDirection:"row",
                justifyContent:"space-around",
                alignItems:"center",               
            }}
            className="h-16 mt-8"
            >
                <View style={{
                    flexDirection:"row",
                    justifyContent:"center",
                    alignItems:"center",
                    flex:1,
                    
                }}>
                    {
                        title!=="Messages"&&title!=="Chat"?
                        <TouchableOpacity 
                        onPress={() => navigation.openDrawer()}
                        style={{margin:5}}
                        >
                            <Icon name="align-center" size={25} style={{marginRight: 0, color:"#309bd1"}} />
                        </TouchableOpacity>:
                        <TouchableOpacity 
                        onPress={() => navigation.goBack()}
                        style={{margin:5,alignSelf:"flex-start"}}
                        >
                            <Ionicons name="chevron-back-outline" size={25} style={{marginRight: 0, color:"#309bd1"}} />
                        </TouchableOpacity>
                    }
                    
                    <Text style={{fontSize:19,color:"#309bd1"}} >{title} </Text>
                </View>
                {
                    title!=="Messages"&&title!=="Chat"?
                    <View style={{
                        flexDirection:"row",
                        width:20,
                        justifyContent:"center",
                        alignItems:"center",
                        flex:1,
                        borderColor:"black"
                    }}>
                        <TouchableOpacity 
                            onPress={() => navigation.navigate('notifications')}
                            style={{margin:5}}
                        >
                            <Icon name="bell" size={25} style={{marginRight: 0, color:"#309bd1"}} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Chatbox')}
                            style={{marginLeft:30}}
                        >
                            <Icon name="message-circle" size={25} style={{marginRight: 0, color:"#309bd1"}} />
                        </TouchableOpacity>
                    </View>:null
                }
                
            </View>
        </View>
    );
}
export default Header;