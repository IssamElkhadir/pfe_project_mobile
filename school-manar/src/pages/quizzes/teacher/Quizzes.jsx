import { View, Text, ScrollView, TouchableOpacity, Modal, Pressable, Image } from "react-native";
import React, { useLayoutEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux"
import {
  AdjustmentsVerticalIcon,
  ClockIcon,
  ListBulletIcon,
  MagnifyingGlassIcon,
  TrashIcon,
} from "react-native-heroicons/outline";
import { Switch, TextInput } from "react-native-paper";
import Toast from 'react-native-toast-message';
import { useEffect } from "react";
import { getQuizzes, reset } from "../../../../redux/QuizzesSlice.js"
import SearchModal from "../../../../components/quizzes/SearchModal.jsx"
import DeleteModal from "../../../../components/quizzes/DeleteModal.jsx";
import Loading2 from "../../../Loading2.jsx";
import QuizCard from "../../../../components/quizzes/QuizCard.jsx";


const Quizzes = () => {
  const dispatch = useDispatch();

  const { quizzes, isLoading, isSuccess, isError, message } = useSelector(state => state.quizzes);

  useEffect(() => {
    dispatch(getQuizzes());
  }, []);

  useEffect(() => {
    setFilteredQuizzes(quizzes);
  }, [quizzes]);
  const [searchText, setSearchText] = useState("");
  const [searchOption, setSearchOption] = useState("by name");
  const [modalVisible_search, setModalVisible_search] = useState(false);
  const [modalVisible_delete, setModalVisible_delete] = useState(false);
  const [isSwitchOn, setIsSwitchOn] = useState(false);
  const [activeOption, setActiveOption] = useState('Latest');

  const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);
  const [filteredQuizzes, setFilteredQuizzes] = useState()

  const handleSearch = (text) => {
    setSearchText(text);

    if (text.trim().length === 0) {
      setFilteredQuizzes(quizzes);
      return;
    }
    if (searchOption === 'by name') {
      const filtered_quizzes = quizzes.filter((quiz) =>
        quiz.quiz_name.trim().toLowerCase().includes(text.trim().toLowerCase())
      );
      setFilteredQuizzes(filtered_quizzes);
      return;
    }
    if (searchOption === 'by number of questions') {
      const filtered_quizzes = quizzes.filter(
        (quiz) => quiz.num_questions == text
      );
      setFilteredQuizzes(filtered_quizzes);
      return;
    }
    if (searchOption === 'by duration') {
      const filtered_quizzes = quizzes.filter(
        (quiz) => quiz.duration == text
      );
      setFilteredQuizzes(filtered_quizzes);
      return;
    }
  };
  const handleSearchOption = (option) => {
    setSearchOption(option);
  };

  const handleSearchModalVisibily = () => {
    setModalVisible_search(!modalVisible_search);
  };
  const handleDeleteModalVisibily = () => {
    setModalVisible_delete(!modalVisible_delete);
  };
  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions(
      {
        headerShown: false,
      },
      []
    );
  });
  let content;
  if (isError === false && isSuccess === true && message === "" && isLoading === false) {
    content = <View
      className="w-screen"
    >
      {modalVisible_search && (
        <SearchModal
          modalVisible={modalVisible_search}
          handleSearchOption={handleSearchOption}
          handleSearchModalVisibily={handleSearchModalVisibily}
          searchOption={searchOption}
        />
      )}
      {
        modalVisible_delete && (
          <DeleteModal 
            modalVisible={modalVisible_delete}
            handleDeleteModalVisibily = {handleDeleteModalVisibily}
            subject = "quiz"
          />
        )
      }

      <View className="flex-row items-center space-x-2 bg-white  rounded-lg mt-4 px-4 mx-3">
        <View className="flex-1 flex-row items-center space-x-2 ">
          <MagnifyingGlassIcon />
          <TextInput
            placeholder="search "
            caretHidden={false}
            value={searchText}
            onChangeText={handleSearch}
            selectionColor="#000"
            activeUnderlineColor="transparent"
            underlineColor="transparent"
            keyboardType="default"
            className="w-[80%] focus:underline-offset-0 focus:outline-0 focus:border-none bg-transparent border-none text-gray-900"
          />
        </View>
        <AdjustmentsVerticalIcon onPress={handleSearchModalVisibily} />
      </View>
      <View className="rounded-3xl bg-white mt-5 w-full">
        <View className="flex-row justify-center items-center mt-5">
          <View className="w-20 h-1 bg-blue-500 rounded-lg"></View>
        </View>
        <View className="h-[600px]">
          <View className="flex flex-row space-x-1.5 mb-4 ml-2 mt-2">
            <TouchableOpacity
              className={`px-4 py-2 rounded-xl ${activeOption === 'Latest' ? 'bg-gray-300' : 'bg-gray-100'
                }`}
              onPress={() => setActiveOption('Latest')}
            >
              <Text className="text-gray-700 font-bold">Latest</Text>
            </TouchableOpacity>
            <TouchableOpacity
              className={`px-4 py-2 rounded-xl ${activeOption === 'Oldest' ? 'bg-gray-300' : 'bg-gray-100'
                }`}
              onPress={() => setActiveOption('Oldest')}
            >
              <Text className="text-gray-700 font-bold">Oldest</Text>
            </TouchableOpacity>
          </View>
          <ScrollView
            vertical
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1 }}
            className="flex flex-col space-y-4 p-4 h-auto"
          >
            {filteredQuizzes &&
              filteredQuizzes.map((quiz) => (
                <QuizCard key={quiz.id} id={quiz.id} quizName={quiz.quiz_name} quizDuration={quiz.duration} quizNbQuestions={quiz.num_questions} handleDeleteModalVisibily= {handleDeleteModalVisibily}/>
              ))}
          </ScrollView>
        </View>
      </View>
    </View>
  }
  else if (isLoading === true && isError === false && message === "" && isSuccess === false) {
    content =
      <View
        className="w-screen"
      ><Loading2 />
      </View>
  }else if (isError === false && isSuccess === false && message !== "" && isLoading === false) {
    content =
      <View
        className="w-screen h-screen bg-white flex justify-center items-center"
      ><Text className="text-gray-900 font-bold text-xl">Something goes wrong !!!</Text>
      </View>
  }
  return (
    <View className="bg-[#e6f5fc]">
      {content}
    </View>
  );
};

export default Quizzes;
