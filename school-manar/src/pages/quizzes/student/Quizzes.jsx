import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Modal,
    Pressable,
    Image,
    Alert,
    ActivityIndicator,
    RefreshControl,
} from "react-native";
import React, { useLayoutEffect, useState } from "react";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getQuizzes_student, getQuizzes_student_for_reloading_quizzes, reset } from "../../../redux/QuizzesSlice";
import SearchModal from "../../../components/quizzes/SearchModal";
import DeleteModal from "../../../components/quizzes/DeleteModal.jsx";
import Loading2 from "../../Loading2.jsx";
import QuizCard from "../../../components/quizzes/student/QuizCard.jsx";
import axios from "axios";
import SearchInputContainer from "../../../components/quizzes/student/SearchInputContainer";
import { useCallback } from "react";


const baseUrl = "https://b734-41-249-65-61.ngrok-free.app"

const Quizzes = () => {
    const navigation = useNavigation();
    // const data = useSelector((state) => state.auth);
    const user_id = 3

    useLayoutEffect(() => {
        navigation.setOptions(
            {
                headerShown: false,
            },
            []
        );
    });
    let showContinueOneTime = true;
    const dispatch = useDispatch();

    const { quizzes, isLoading, isSuccess, isError, message } = useSelector(
        (state) => state.quizzes
    );


    useEffect(() => {
        dispatch(getQuizzes_student(user_id));
        setActiveQuiz()
    }, []);
    // force reloading quizzes when going back
    useFocusEffect(
        React.useCallback(() => {
            dispatch(getQuizzes_student(user_id));
            setActiveQuiz()
        }, [])
    );

    useEffect(() => {
        setFilteredQuizzes(quizzes);
        console.log(quizzes)
    }, [quizzes]);
    // when the user scroll top the quizzes list the list will reload
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(() => {
        dispatch(getQuizzes_student_for_reloading_quizzes(user_id))
        setRefreshing(true);
        setTimeout(() => {
            setRefreshing(false);
        }, 2000);
    }, []);

    const [searchText, setSearchText] = useState("");
    const [searchOption, setSearchOption] = useState("by name");
    const [modalVisible_search, setModalVisible_search] = useState(false);
    const [modalVisible_delete, setModalVisible_delete] = useState(false);
    const [activeQuiz, setActiveQuiz] = useState();
    const [iconReloadQuizzesVisibility, setIconReloadQuizzesVisibility] = useState(false)
    const [activeQuiz_delete, setActiveQuiz_delete] = useState();

    const [filteredQuizzes, setFilteredQuizzes] = useState();

    const handleSearch = (text) => {
        setSearchText(text);

        if (text.trim().length === 0) {
            setFilteredQuizzes(quizzes);
            return;
        }
        if (searchOption === "by name") {
            const filtered_quizzes = quizzes.filter((quiz) =>
                quiz.quiz_name.trim().toLowerCase().includes(text.trim().toLowerCase())
            );
            setFilteredQuizzes(filtered_quizzes);
            return;
        }
        if (searchOption === "by number of questions") {
            const filtered_quizzes = quizzes.filter(
                (quiz) => quiz.num_questions == text
            );
            setFilteredQuizzes(filtered_quizzes);
            return;
        }
        if (searchOption === "by duration") {
            const filtered_quizzes = quizzes.filter((quiz) => quiz.duration == text);
            setFilteredQuizzes(filtered_quizzes);
            return;
        }

        // if (searchOption === "by date") {
        //     const filtered_quizzes = quizzes.filter((quiz) => {
        //         const searchTerm = text.toLowerCase();
        //         return (
        //             quiz.start_at.toLowerCase().includes(searchTerm) ||
        //             quiz.end_at.toLowerCase().includes(searchTerm)
        //         );
        //     });

        //     setFilteredQuizzes(filtered_quizzes);
        //     return;
        // }
    };
    const handleSearchOption = (option) => {
        setSearchOption(option);
    };

    const handleSearchModalVisibily = () => {
        setModalVisible_search(!modalVisible_search);
    };
    const handleDeleteModalVisibily = (quiz_session_id) => {
        setModalVisible_delete(!modalVisible_delete);
        setActiveQuiz_delete(quiz_session_id)
    };
    const handleDeleteModalVisibily_for_cancel = () => {
        setModalVisible_delete(!modalVisible_delete);
    };
    const handleDeleteQuizSession = async () => {
        setModalVisible_delete(!modalVisible_delete);
        if (user_id && activeQuiz_delete) {
            setIconReloadQuizzesVisibility(true);
            try {
                const requestData = {
                    user_id,
                    quiz_session_id: activeQuiz_delete
                };
                const config = {
                    headers: {
                        'If-Modified-Since': undefined,
                    },
                };
                const response = await axios.post(
                    `${baseUrl}/api/quizzes/delete_quiz_session_by_student`,
                    requestData,
                    config
                );
                console.log("message =>", response.data.message);
                dispatch(getQuizzes_student_for_reloading_quizzes(user_id))
                    .then(() => setIconReloadQuizzesVisibility(false))
                    .catch((error) => {
                        console.error(error);
                        setIconReloadQuizzesVisibility(false);
                    });
            } catch (error) {
                console.error(error);
            }
        }
    }
    const handleActiveQuiz = (id) => {
        const quiz = quizzes.find((quiz) => quiz.id === id);
        setActiveQuiz(quiz.id);
    };

    const StartQuiz = () => {
        if (!activeQuiz) {
            return Alert.alert(
                'please select a quiz',
            );
        }
        const quiz = quizzes.find((quiz) => quiz.id == activeQuiz);
        navigation.navigate("QuizPassing", { quiz: quiz, status: 'start_quiz', user_id: user_id });
    };


    let content;
    if (
        isError === false &&
        isSuccess === true &&
        message === "" &&
        isLoading === false &&
        quizzes.length > 0
    ) {
        content = (
            <View className="w-screen">
                {modalVisible_search && (
                    <SearchModal
                        modalVisible={modalVisible_search}
                        handleSearchOption={handleSearchOption}
                        handleSearchModalVisibily={handleSearchModalVisibily}
                        searchOption={searchOption}
                        subject={"search_quiz"}
                    />
                )}
                {modalVisible_delete && (
                    <DeleteModal
                        modalVisible={modalVisible_delete}
                        handleDeleteModalVisibily={handleDeleteModalVisibily}
                        subject="quiz"
                        handleDeleteQuizSession={handleDeleteQuizSession}
                        handleDeleteModalVisibily_for_cancel={handleDeleteModalVisibily_for_cancel}
                    />
                )}

                <SearchInputContainer searchText={searchText} handleSearch={handleSearch} handleSearchModalVisibily={handleSearchModalVisibily} />
                <View className="rounded-3xl bg-white mt-5 w-full">
                    <View className="flex-row justify-center items-center mt-5">
                        <View className="w-20 h-1 bg-blue-500 rounded-lg"></View>
                    </View>
                    <View className="h-[600px]">
                        <ScrollView
                            refreshControl={
                                <RefreshControl size={20} refreshing={refreshing} onRefresh={onRefresh} />
                            }
                            vertical
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ flexGrow: 1 }}
                            className="flex flex-col space-y-4 p-4 h-auto"
                        >
                            <ActivityIndicator color={"black"} size={20} className={`mb-3 ${iconReloadQuizzesVisibility ? 'block' : 'hidden'}`} />
                            {filteredQuizzes &&
                                filteredQuizzes.map((quiz) => {
                                    if (quiz.quizsession_start_at == "null" || quiz.quizsession_start_at == null) {
                                        return (
                                            <QuizCard
                                                is_continue_quiz={false}
                                                activeQuiz={activeQuiz}
                                                start_at={quiz.start_at}
                                                end_at={quiz.end_at}
                                                handleActiveQuiz={handleActiveQuiz}
                                                key={quiz.id}
                                                id={quiz.id}
                                                quizName={quiz.quiz_name}
                                                quizDuration={quiz.duration}
                                                quizNbQuestions={quiz.num_questions}
                                                handleDeleteModalVisibily={handleDeleteModalVisibily}
                                            />
                                        )
                                    }
                                })}

                            {filteredQuizzes &&
                                filteredQuizzes.map((quiz) => {
                                    if (quiz.quizsession_start_at !== "null" && quiz.quizsession_start_at !== null) {
                                        if (showContinueOneTime === true) {
                                            showContinueOneTime = false;
                                            return (
                                                <Text className="text-xl font-bold text-gray-900">Continue Quiz</Text>
                                            )
                                        }
                                    }
                                })}
                            {filteredQuizzes &&
                                filteredQuizzes.map((quiz) => {
                                    if (quiz.quizsession_start_at !== "null" && quiz.quizsession_start_at !== null) {
                                        return (
                                            <QuizCard
                                                user_id={user_id}
                                                quiz={quiz}
                                                is_continue_quiz={true}
                                                activeQuiz={activeQuiz}
                                                start_at={quiz.start_at}
                                                end_at={quiz.end_at}
                                                handleActiveQuiz={handleActiveQuiz}
                                                key={quiz.id}
                                                id={quiz.id}
                                                quizName={quiz.quiz_name}
                                                quizsession_started_at={quiz.quizsession_start_at}
                                                quizsession_ended_at={quiz.quizsession_end_at}
                                                quizsessionDuration={quiz.quiz_session_duration}
                                                quizNbQuestions={quiz.num_questions}
                                                handleDeleteModalVisibily={handleDeleteModalVisibily}
                                            />
                                        )
                                    }
                                })}

                        </ScrollView>
                        <TouchableOpacity
                            onPress={StartQuiz}
                            className="bg-blue-500 rounded-lg py-3 px-6 mx-5 mb-1"
                        >
                            <Text className="text-white text-base font-bold text-center">
                                Start a quiz
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    } else if (
        isLoading === true &&
        isError === false &&
        message === "" &&
        isSuccess === false
    ) {
        content = (
            <View className="w-screen">
                <Loading2 />
            </View>
        );
    } else if (
        isError === false &&
        isSuccess === true &&
        message === "" &&
        isLoading === false &&
        quizzes.length == 0
    ) {
        content = (
            <View className="w-screen h-screen bg-white  flex justify-center items-center">
                <Text className="text-gray-900 font-bold mb-32 text-xl">
                    There is no quiz for you yet .
                </Text>
            </View>
        );
    }
    else if (
        isError === true &&
        isSuccess === false &&
        message !== "" &&
        isLoading === false
    ) {
        content = (
            <View className="w-screen h-screen bg-white flex justify-center items-center">
                <Text className="text-gray-900 font-bold mb-32 text-xl">
                    Something goes wrong !!!
                </Text>
            </View>
        );
    }
    return <View className="bg-[#e6f5fc]">{content}</View>;
};

export default Quizzes;
