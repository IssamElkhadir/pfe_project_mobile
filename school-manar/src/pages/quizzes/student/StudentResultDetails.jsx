import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import React from "react";
import { ArrowLeftIcon } from "react-native-heroicons/outline";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getCorrectedQuestions } from "../../../redux/studentResults";
import Loading2 from "../../Loading2";

const StudentResultDetails = ({ route }) => {
  const navigate = useNavigation();
  const dispatch = useDispatch();
  const { result  ,user_id} = route.params;
  const { correctedQuestions, isLoading, isError, isSuccess, message } = useSelector((state) => state.studentResults);

  useEffect(()=>{
    dispatch(getCorrectedQuestions({quiz_session_id : result.id , user_id}))
  } , [])
  

  
  let content ;
  if (
    isError === false &&
    isSuccess === true &&
    message === "" &&
    isLoading === false
  ) {
    content = (
      <>
        <View className="flex flex-row justify-between items-center p-4">
        <TouchableOpacity
          className="p-4 rounded-full bg-[#e6f5fc]"
          onPress={() => navigate.goBack()}
        >
          <ArrowLeftIcon size={20} color="blue" />
        </TouchableOpacity>
      </View>
      <View className="h-[700px]">
        <ScrollView vertical showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1 }}
          className="flex flex-col space-y-4 p-4 h-auto"
        >
          <View className="flex flex-row items-center space-x-1">
            <View className="bg-red-600 w-5 h-5"></View>
            <Text className="pt-2">The question in red indicates that your answer is incorrect.</Text>
          </View>
          <View className="flex flex-row items-center space-x-1">
            <View className="bg-green-600 w-5 h-5"></View>
            <Text className="pt-2">The question in red indicates that your answer is correct .</Text>
          </View>
          <View className="flex flex-row items-center space-x-1">
            <View className="bg-black w-5 h-5"></View>
            <Text className="pt-2">The question in black will be corrected by your teacher.</Text>
          </View>
          <View className="flex flex-col gap-1">
            {correctedQuestions && correctedQuestions.map((question, index) => (
              <View className=" p-2 rounded-lg">
                <Text className={`font-bold ${question.is_response_correct === 1 ? "text-green-600" : (question.is_response_correct === null ? "text-black" : "text-red-600")}`}>{index + 1} - {question.question_text}</Text>
                <View className="flex flex-row space-x-2 mt-2">
                  <Text>Your response : </Text>
                  <Text className="w-[220px]">
                    {(question.question_type === "reorder") ?
                      question.user_response.map((res) => (
                        res + " "
                      )) : (question.question_type === "multiple_choice_multipleAnswer") ? question.user_response.map((res) => (
                        res + " , "
                      )) : question.user_response
                    }
                  </Text>
                </View>
                <View className="flex flex-row space-x-2 mt-2">
                  <Text>The correct response : </Text>
                  <Text className="w-[220px]">
                    {(question.question_type === "reorder") ?
                      question.correct_answer.map((res) => (
                        res + " "
                      )) : (question.question_type === "multiple_choice_multipleAnswer") ? question.correct_answer.map((res) => (
                        res + " , "
                      )) : question.correct_answer
                    }
                  </Text>
                </View>
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
      </>
    );
  } else if (
    isLoading === true &&
    isError === false &&
    message === "" &&
    isSuccess === false
  ) {
    content = (
      <View className="w-screen">
        <Loading2 />
      </View>
    );
  } 
  // else if (
  //   isError === false &&
  //   isSuccess === true &&
  //   message === "" &&
  //   isLoading === false 
  // ) {
  //   content = (
  //     <View className="w-screen h-screen bg-white  flex justify-center items-center">
  //       <Text className="text-gray-900 font-bold mb-32 text-xl">
  //         There is no result yet .
  //       </Text>
  //     </View>
  //   );
  // }
  else if (
    isError === true &&
    isSuccess === false &&
    message !== "" &&
    isLoading === false
  ) {
    content = (
      <View className="w-screen h-screen bg-white flex justify-center items-center">
        <Text className="text-gray-900 font-bold mb-32 text-xl">
          Something goes wrong !!!
        </Text>
      </View>
    );
  }
  return (
    <View className="bg-white min-h-screen">
      {content}
    </View>
  );
};

export default StudentResultDetails;
