import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Animated,
  Alert,
} from "react-native";
import React, { useEffect, useState } from "react";
import {
  ArrowLeftIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "react-native-heroicons/outline";
import { useNavigation } from "@react-navigation/native";
import QuestionContainer from "../../../components/quizzes/student/QuestionContainer";
import { useRef } from "react";
import {useDispatch, useSelector} from "react-redux"
import { getQuestions_student } from "../../../redux/QuestionsSlice";
import { getStudentResponses } from "../../../redux/studentResponses";
import Loading2 from "../../Loading2";
import axios from "axios";


const baseUrl = "https://b734-41-249-65-61.ngrok-free.app"

const QuizPassing = ({ route }) => {
  const { quiz , status , user_id } = route.params;
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchData = async () => {
      dispatch(getQuestions_student({ quizId: quiz.quiz_id, isRandomQuestions: quiz.isRandomQuestions, isRandomAnswers: quiz.isRandomAnswers }));
      dispatch(getStudentResponses({user_id , quiz_session_id : quiz.id}))
      try {
        console.log(quiz.id)
        const requestData = {
          user_id,
          quiz_session_id: quiz.id,
          duration,
        };
        const config = {
          headers: {
            'If-Modified-Since': undefined,
          },
        };
        const response = await axios.post(
          `${baseUrl}/api/quizzes/quiz_passing/saveStartedQuiz`,
          requestData,
          config
        );
        console.log("message =>", response.data.message);
      } catch (error) {
        console.error(error);
      }
    };
  
    fetchData();
    if(studentResponses){
      setAnswers(studentResponses)
    }
  }, []);
  
  const {questions ,options ,  isLoading, isSuccess, isError, message} = useSelector((state)=> state.questions)
  const {studentResponses} = useSelector((state)=> state.studentResponses);

  const [answers, setAnswers] = useState({});
  const navigate = useNavigation();
  
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [activeQuestionId , setActiveQuestionId] = useState();
  const handleUserAnswer = (quiz_session_id , question_id, answer, question_type) => {
    setActiveQuestionId(question_id);
    setAnswers((prevAnswers) => ({
      ...prevAnswers,
      [question_id]: {
        quiz_session_id : quiz_session_id , 
        question_id: question_id,
        question_type: question_type,
        response: answer,
      },
    }));
  };
  useEffect(() => {
    const saveAnswers = async (question_id , question_type ,user_response , quiz_session_id) => {
      try {
        const requestData = {
          user_id : user_id ,
          question_id,
          question_type ,
          response : user_response,
          quiz_session_id
        };
        const config = {
          headers: {
            "If-Modified-Since": undefined,
          },
        };
        const response = await axios.post(
          `${baseUrl}/api/quizzes/quiz_passing/saveAnswers`,
          requestData,
          config
        );
        console.log("message =>", response.data.message);
      } catch (error) {
        console.error(error);
      }
    };
    if (Object.keys(answers).length > 0) {
      if (activeQuestionId && answers[activeQuestionId]) {
        const {
          question_id,
          question_type,
          response,
          quiz_session_id,
        } = answers[activeQuestionId];
        if (response !== "") {
          saveAnswers(
            question_id,
            question_type,
            response,
            quiz_session_id
          );
        }
      }
    }
  }, [answers]);

  // start quiz => get the quiz initial duration , continue quiz => get the quiz session duration
  const [duration, setDuration] = useState(status=== "start_quiz" ? quiz.duration * 60 : quiz.quiz_session_duration);
  const fadeAnim = useState(new Animated.Value(0))[0];
  const AnimatedFunc = () =>{
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 800, // Adjust the duration as needed
      useNativeDriver: true,
    }).start();
    
  }
  AnimatedFunc();
  const animateQuestionContainer = () => {
    fadeAnim.setValue(0); // Reset the animation value
    AnimatedFunc();
  };

  useEffect(() => {
    animateQuestionContainer();
  }, [currentQuestion]);
  useEffect(() => {
    const interval = setInterval(() => {
      if (duration > 0) {
        setDuration(duration - 1);
      }
      if (duration === 0) {
        navigate.goBack();
        const sendRequest = async ()=>{
          try {
            const requestData = {
              user_id,
              quiz_session_id: quiz.id,
              duration,
              completed : true 
            };
            const config = {
              headers: {
                'If-Modified-Since': undefined,
              },
            };
            const response = await axios.post(
              `${baseUrl}/api/quizzes/quiz_passing/saveQuizResult`,
              requestData,
              config
            );
            console.log("message =>",response.data.message);
          } catch (error) {
            console.error(error);
          }
        }
        sendRequest()
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [duration]);

  const formatDuration = (duration) => {
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor((duration % 3600) / 60);
    const seconds = duration % 60;

    const formattedHours = hours.toString().padStart(2, "0");
    const formattedMinutes = minutes.toString().padStart(2, "0");
    const formattedSeconds = seconds.toString().padStart(2, "0");

    return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
  };

  const goBack = () => {
    Alert.alert(
      "Confirmation",
      "Are you sure you want to go back?",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            navigate.goBack();
            try {
              const requestData = {
                user_id,
                quiz_session_id: quiz.id,
                duration,
              };
              const config = {
                headers: {
                  'If-Modified-Since': undefined,
                },
              };
              const response = await axios.post(
                `${baseUrl}/api/quizzes/quiz_passing/saveDuration`,
                requestData,
                config
              );
              console.log("message =>",response.data.message);
            } catch (error) {
              console.error(error);
            }
          },
        },
      ],
      { cancelable: true }
    );
  };

  const goToPreviousQuestion = () => {
    if (currentQuestion > 0) {
      setCurrentQuestion(currentQuestion - 1);
    } else {
      setCurrentQuestion(questions.length - 1);
    }
  };

  const goToNextQuestion = () => {
    if (currentQuestion < questions.length - 1) {
      setCurrentQuestion(currentQuestion + 1);
    } else {
      setCurrentQuestion(0);
    }
  };

  const goToQuestion = (questionIndex) => {
    setCurrentQuestion(questionIndex);
  };

  const submitQuiz = () => {
    Alert.alert(
      "Confirmation",
      "Are you sure you want submit the quiz?",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            navigate.goBack();
            try {
              const requestData = {
                user_id,
                quiz_session_id: quiz.id,
                duration,
                completed : true 
              };
              const config = {
                headers: {
                  'If-Modified-Since': undefined,
                },
              };
              const response = await axios.post(
                `${baseUrl}/api/quizzes/quiz_passing/saveQuizResult`,
                requestData,
                config
              );
              console.log("message =>",response.data.message);
            } catch (error) {
              console.error(error);
            }
          },
        },
      ],
      { cancelable: true }
    );
  };
  let content;
    if (
        isError === false &&
        isSuccess === true &&
        message === "" &&
        isLoading === false 
    ) {
        content = (
          <View className="flex flex-1 bg-white">
          <View className="flex flex-row justify-between items-center p-4">
              <TouchableOpacity
                className="p-4 rounded-full bg-[#e6f5fc]"
                onPress={goBack}
              >
                <ArrowLeftIcon size={20} color="blue" />
              </TouchableOpacity>
              <Text className="text-lg font-bold text-gray-700">
                {quiz.quiz_name}
              </Text>
              <View>
                <Text className={`${duration < 15 ? 'text-red-600' : 'text-black'}`}>{formatDuration(duration)}</Text>
              </View>
            </View>
            <View className="relative rounded-3xl bg-white w-full shadow shadow-gray-400 p-4 h-screen">
              <View className="flex-row justify-center items-center mt-5">
                <View className="w-20 h-1 bg-blue-500 rounded-lg"></View>
              </View>
              <View className="flex flex-row justify-center items-center mt-5">
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {questions.map((_, index) => (
                    <TouchableOpacity
                      key={index}
                      className={`px-4 py-3 mr-2 rounded-full ${index === currentQuestion ? "bg-blue-500 " : "bg-gray-300"
                        }`}
                      onPress={() => goToQuestion(index)}
                    >
                      <Text
                        className={`text-white ${index === currentQuestion ? "font-bold" : "font-normal"
                          }`}
                      >
                        {index + 1}
                      </Text>
                    </TouchableOpacity>
                  ))}
                </ScrollView>
              </View>
      
              {/* question container */}
              <Animated.View
                style={{
                  opacity: fadeAnim,
                }}
              >
                <QuestionContainer
                  quiz_session_id = {quiz.id}
                  key={questions[currentQuestion].id}
                  question_id={questions[currentQuestion].id}
                  question_text={questions[currentQuestion].question_text}
                  question_type={questions[currentQuestion].question_type}
                  multimedia={questions[currentQuestion].multimedia}
                  multimedia_type={questions[currentQuestion].multimedia_type}
                  question_explanation={
                    questions[currentQuestion].question_explanation
                  }
                  
                  question_options={options}
                  handleUserAnswer={handleUserAnswer}
                  user_answer = {answers[questions[currentQuestion].id] ? answers[questions[currentQuestion].id].response: null} 
                />
              </Animated.View>
      
              <View className=" absolute bottom-52 w-full flex-row justify-between items-center p-4 ml-5">
                <TouchableOpacity
                  className={`p-4 rounded-full ${currentQuestion === 0 ? "bg-gray-200" : "bg-[#e6f5fc]"
                    }`}
                  onPress={goToPreviousQuestion}
                >
                  <ChevronLeftIcon size={20} color="blue" />
                </TouchableOpacity>
                <TouchableOpacity
                  className={`bg-blue-500 px-6 py-3 rounded-md`}
                  onPress={submitQuiz}
                >
                  <Text className="text-white text-center">Submit Quiz</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  className={`p-4 rounded-full ${currentQuestion === questions.length - 1
                      ? "bg-gray-200"
                      : "bg-[#e6f5fc]"
                    }`}
                  onPress={goToNextQuestion}
                >
                  <ChevronRightIcon size={20} color="blue" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        );
    } else if (
        isLoading === true &&
        isError === false &&
        message === "" &&
        isSuccess === false
    ) {
        content = (
            <View className="w-screen">
                <Loading2 />
            </View>
        );
    }
    else if (
        isError === true &&
        isSuccess === false &&
        message !== "" &&
        isLoading === false
    ) {
        content = (
            <View className="w-screen h-screen bg-white flex justify-center items-center">
                <Text className="text-gray-900 font-bold mb-32 text-xl">
                    Something goes wrong !!!
                </Text>
            </View>
        );
    }
  return (
    <>
      {content}
    </>
  );
};

export default QuizPassing;
