import { View, Text, ScrollView, TouchableOpacity, Image, RefreshControl } from 'react-native'
import React, { useEffect } from 'react'
import { useFocusEffect, useNavigation } from '@react-navigation/native'
import { useLayoutEffect } from 'react'
import { TextInput } from 'react-native-paper'
import { useState } from 'react'
import SearchModal from '../../../components/quizzes/SearchModal'
import FilterOptions_StudentResults from '../../../components/quizzes/student/FilterOptions_StudentResults'
import SearchInputContainer from '../../../components/quizzes/student/SearchInputContainer'
import StudentResultCard from '../../../components/quizzes/student/StudentResultCard'
import { useDispatch, useSelector } from "react-redux"
import { getStudentResults ,get_student_results_for_reloading , reset} from '../../../redux/studentResults'
import Loading2 from '../../Loading2'
import { useCallback } from 'react'

const StudentQuizzesResults = () => {
  const user_id = 3;
  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions(
      {
        headerShown: false,
      },
      []
    );
  });
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getStudentResults(user_id));
    return ()=>{
      dispatch(reset())
    }
  }, []);

  const { studentResults, isLoading, isError, isSuccess, message } = useSelector((state) => state.studentResults);

  const [modalVisible_search, setModalVisible_search] = useState(false);
  const [activeFilterOption, setActiveFilterOption] = useState("");
  const [searchText, setSearchText] = useState("");
  const [searchOption, setSearchOption] = useState("by name");
  const [filtered_studentResults, setFiltered_studentResults] = useState(studentResults);
  const [refreshing, setRefreshing] = useState(false);

  const handleSearchModalVisibily = () => {
    setModalVisible_search(!modalVisible_search);
  };

  const handleActiveFilterOption = (option_id) => {
    setActiveFilterOption(option_id);
  };

  const handleSearch = (text) => {
    setSearchText(text);
    if (text.trim().length === 0) {
      setFiltered_studentResults(studentResults);
      return;
    }
    if (searchOption === "by name") {
      const filtered_results = studentResults.filter((result) =>
        result.quiz_name.trim().toLowerCase().includes(text.trim().toLowerCase())
      );
      setFiltered_studentResults(filtered_results);
      return;
    }
    if (searchOption === "by score") {
      const filtered_results = studentResults.filter(
        (result) => result.score == text
      );
      setFiltered_studentResults(filtered_results);
      return;
    }
  };

  const handleSearchOption = (option) => {
    setSearchOption(option);
  };

  const onRefresh = useCallback(() => {
    dispatch(get_student_results_for_reloading(user_id));
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  useEffect(() => {
    setFiltered_studentResults(studentResults);
  }, [studentResults])

  useEffect(() => {
    const updateFilteredStudentResults = () => {
      if (filtered_studentResults) {
        let clonedStudentResults = [...filtered_studentResults];
        switch (activeFilterOption) {
          case "Newest":
            clonedStudentResults.sort((a, b) =>
              new Date(b.start_at) - new Date(a.start_at)
            );
            break;
          case "Oldest":
            clonedStudentResults.sort((a, b) =>
              new Date(a.start_at) - new Date(b.start_at)
            );
            break;
          case "Last 3 days":
            const threeDaysAgo = new Date();
            threeDaysAgo.setDate(threeDaysAgo.getDate() - 3);
            clonedStudentResults = clonedStudentResults.filter(
              (result) => new Date(result.start_at) >= threeDaysAgo
            );
            break;
          case "Last week":
            const oneWeekAgo = new Date();
            oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
            clonedStudentResults = clonedStudentResults.filter(
              (result) => new Date(result.start_at) >= oneWeekAgo
            );
            break;
          case "Last month":
            const oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
            clonedStudentResults = clonedStudentResults.filter(
              (result) => new Date(result.start_at) >= oneMonthAgo
            );
            break;
          case "Last 3 months":
            const threeMonthsAgo = new Date();
            threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);
            clonedStudentResults = clonedStudentResults.filter(
              (result) => new Date(result.start_at) >= threeMonthsAgo
            );
            break;
          case "Last year":
            const oneYearAgo = new Date();
            oneYearAgo.setFullYear(oneYearAgo.getFullYear() - 1);
            clonedStudentResults = clonedStudentResults.filter(
              (result) => new Date(result.start_at) >= oneYearAgo
            );
            break;
          case "Highest score":
            clonedStudentResults.sort((a, b) => b.score - a.score);
            break;
          case "Lowest score":
            clonedStudentResults.sort((a, b) => a.score - b.score);
            break;
          default:
            break;
        }
        setFiltered_studentResults(clonedStudentResults);
      }
    };
    updateFilteredStudentResults();
  }, [activeFilterOption]);

  let content;
  if (
    isError === false &&
    isSuccess === true &&
    message === "" &&
    isLoading === false
  ) {
    content = (
      <>
        {modalVisible_search && (
          <SearchModal
            modalVisible={modalVisible_search}
            handleSearchOption={handleSearchOption}
            handleSearchModalVisibily={handleSearchModalVisibily}
            searchOption={searchOption}
            subject={"search_student_result"}
          />
        )}
        <View className="w-screen">
          <SearchInputContainer searchText={searchText} handleSearch={handleSearch} handleSearchModalVisibily={handleSearchModalVisibily} />
          <View className="rounded-3xl bg-white mt-5 w-full">
            <View className="flex-row justify-center items-center mt-5">
              <View className="w-20 h-1 bg-blue-500 rounded-lg"></View>
            </View>
            <View className="h-[600px]">
              <ScrollView
                vertical
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}
                refreshControl={
                  <RefreshControl size={20} refreshing={refreshing} onRefresh={onRefresh} />
                }
                className="flex flex-col space-y-4 p-4 h-auto"
              >
                <FilterOptions_StudentResults activeFilterOption={activeFilterOption} handleActiveFilterOption={handleActiveFilterOption} />
                {filtered_studentResults && filtered_studentResults.map((result) => (
                  <StudentResultCard key={result.id} result={result} user_id = {user_id}/>
                ))}
              </ScrollView>
            </View>
          </View>
        </View>
      </>
    );
  } else if (
    isLoading === true &&
    isError === false &&
    message === "" &&
    isSuccess === false
  ) {
    content = (
      <View className="w-screen">
        <Loading2 />
      </View>
    );
  } 
  // else if (
  //   isError === false &&
  //   isSuccess === true &&
  //   message === "" &&
  //   isLoading === false 
  // ) {
  //   content = (
  //     <View className="w-screen h-screen bg-white  flex justify-center items-center">
  //       <Text className="text-gray-900 font-bold mb-32 text-xl">
  //         There is no result yet .
  //       </Text>
  //     </View>
  //   );
  // }
  else if (
    isError === true &&
    isSuccess === false &&
    message !== "" &&
    isLoading === false
  ) {
    content = (
      <View className="w-screen h-screen bg-white flex justify-center items-center">
        <Text className="text-gray-900 font-bold mb-32 text-xl">
          Something goes wrong !!!
        </Text>
      </View>
    );
  }

  return (
    <View className="bg-[#e6f5fc]">
      {content}
    </View>

  )
}

export default StudentQuizzesResults