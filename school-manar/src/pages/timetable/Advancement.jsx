import { ScrollView } from "react-native";
import { TimelineCalendar, } from '@howljs/calendar-kit';
import React from "react";
import moment from "moment";

export default function Advancement(){
  
    return (
      <ScrollView>
            <TimelineCalendar
                viewMode="week"
                allowPinchToZoom
                allowDragToCreate
                start={7}
                end={23}
                //initialDate='2023-05-03'
                onDragCreateEnd={(date) => {
                    console.log(date);
                }}
                isLoading={false}
                unavailableHours={[
                    { start: 0, end: 8 },
                    { start: 19, end: 24 },
                  ]}
                events={[
                    {
                        id: "1",
                        start:"2023-05-03 08:20:00",
                        end: "2023-05-03 13:20:00",
                        title: "string",
                        color: "red",
                      }
                ]}
                onPressEvent={(event)=>{
                    alert("short press");
                }}
                onLongPressEvent={(event)=>{
                    alert("long press")
                }}
                onPressBackground={(date)=>{
                    alert("date selected")
                }}
                onDateChanged={(e)=>{
                    alert("vcbchgf")
                }}
                onPressDayNum={(date)=>{
                    alert(date)
                }}
            />
        </ScrollView>
    )
  
}