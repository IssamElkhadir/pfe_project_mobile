import { View, Text } from 'react-native'
import React from 'react'
import { ActivityIndicator } from 'react-native-paper'

const Loading2 = () => {
  return (
    <View className="bg-white h-screen w-screen flex justify-center items-center">
      <ActivityIndicator size="large" color='purple'/>
    </View>
  )
}

export default Loading2