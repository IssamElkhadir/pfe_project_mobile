import React, { useEffect } from 'react';
import { View, Text, Button, StyleSheet, FlatList } from 'react-native';
import {
  Container,
  Card,
  UserInfo,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  PostTime,
  MessageText,
  TextSection,
} from '../../styles/MessagesStyles';
import socket from "../../utils/socket";
const messages = [
  {
    id: '1',
    userName: 'Jenny Doe',
    userId: '1',
    userImg: 'https://placeimg.com/140/140/any',
    messageTime: '4 mins ago',
    messageText:
      'Hey there, this is my test for a post of my social app in React Native.',
  },
  {
    id: '2',
    userName: 'John Doe',
    userImg: 'https://placeimg.com/140/140/any',
    messageTime: '2 hours ago',
    messageText:
      'Hey there, this is my test for a post of my social app in React Native.',
  },
  {
    id: '3',
    userName: 'Ken William',
    userImg: 'https://placeimg.com/140/140/any',
    messageTime: '1 hours ago',
    messageText:
      'Hey there, this is my test for a post of my social app in React Native.',
  },
  {
    id: '4',
    userName: 'Selina Paul',
    userImg: 'https://placeimg.com/140/140/any',
    messageTime: '1 day ago',
    messageText:
      'Hey there, this is my test for a post of my social app in React Native.',
  },
  {
    id: '5',
    userName: 'Christy Alex',
    userImg: 'https://placeimg.com/140/140/any',
    messageTime: '2 days ago',
    messageText:
      'Hey there, this is my test for a post of my social app in React Native.',
  },
];

const Messages = ({navigation}) => {

  useEffect(() => {
		//dispatch 
	}, []);

  // useEffect(() => {
	// 	socket.on("getText", (messages) => {
  //     //dispatch event 
	// 		//setRooms(rooms);
  //     //push notification

	// 	});
	// }, [socket]);
    return (
      <Container>
        <FlatList 
          data={messages}
          keyExtractor={item=>item.id}
          renderItem={({item}) => (
            <Card onPress={() => navigation.navigate('Chat', {chat: item})}>
              <UserInfo>
                <UserImgWrapper>
                  <UserImg source={{ uri: "https://imgs.search.brave.com/bGvmpgkWdt1O0-I-e7NIZ5nKy-rAWcCTfrmvRi6ENrw/rs:fit:437:225:1/g:ce/aHR0cHM6Ly90c2Ux/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5H/bFhxeGNSOUVtdmlO/NWt1d2FVc01RSGFJ/QiZwaWQ9QXBp" }} />
                </UserImgWrapper>
                <TextSection>
                  <UserInfoText>
                    <UserName>{item.userName}</UserName>
                    <PostTime>{item.messageTime}</PostTime>
                  </UserInfoText>
                  <MessageText>{item.messageText}</MessageText>
                </TextSection>
              </UserInfo>
            </Card>
          )}
        />
      </Container>
    );
};

export default Messages;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
});
