<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;
    protected $table='users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        //user info

     //   'name',
        'name',
        'password',
        'email',
        'type',

        'is_admin',
        'is_teacher',
        'is_tutor', //boolean
        'is_student',

        //common
        'identity_number',
        'first_name', //first name
        'first_name_ar',
        'last_name', //first name
        'last_name_ar',
        'sexe',
        'birth_date',
        'phone',
        'address',
        'address_ar',
        'postal_code',
        'city_id', //dropdown list
        'country_id', //dropdown list
        'nationality_id', //dropdown list
        'currency_id', //dropdown list
        'Remarks', //text area
        'picture', //path of picture
        'created_by_id',
        'edited_by_id',
        'delete_by_id',


        //student
        'student_number',
        'status', //registred or prospect
        'insurance_number',
        'insurance_name',
        'old_school_informations',// informations for  prospect
        'maternal_language_id',
        'birth_city_id',
        'birth_country_id',
        'religion_id',
        'prefered_days',   //for assignement to class
        'paiement_method_id',  //for paiement
        'selected_branche_id', // branche selected
        'selected_level_id',
        'prospecting_source_id', //from what source the student was enrolled in the school
        'school_id',

        //person
        'diploma_id', //dropdown list
        'banque_id', //input text
        'account_number',
        'paypal',
        'salary',
        'hourly_rate',
        'insurance_number',
        'insurance_name',
        'tax_number',
        'status_id', //permanant or contractual dropdown list
        'work_function_id', //dropdown list
        'recruitment_date',
        'departure_date',
        'departure_reasons',
        'school_id',





    ];
    public function sentMessages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function receivedMessages()
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }    

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



}
