<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {



            //common
            $table->string('identity_number')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('first_name_ar')->nullable();
            $table->string('last_name_ar')->nullable();
            $table->string('sexe')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('address_ar')->nullable();
            $table->string('postal_code')->nullable();
            $table->unsignedBigInteger('school_id')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('nationality_id')->nullable();
            $table->boolean('status')->nullable();  // 0=prospect 1=eleve
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->string('picture')->nullable();
            $table->text('Remarks')->nullable();
            $table->foreign('city_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('nationality_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('set null');
            $table->string('insurance_number')->nullable();
            $table->string('insurance_name')->nullable();
            $table->unsignedBigInteger('status_id')->nullable();
            $table->foreign('status_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('currency_id')->references('id')->on('parameters')->onDelete('set null');

            $table->boolean('is_admin')->nullable();
            $table->boolean('is_teacher')->nullable();
            $table->boolean('is_tutor')->nullable();
            $table->boolean('is_student')->nullable();

            //students
            $table->string('student_number')->nullable();
            $table->text('old_school_informations')->nullable();
            $table->unsignedBigInteger('maternal_language_id')->nullable();
            $table->unsignedBigInteger('birth_city_id')->nullable();
            $table->unsignedBigInteger('birth_country_id')->nullable();
            $table->unsignedBigInteger('religion_id')->nullable();
            $table->text('prefered_days')->nullable();
            $table->unsignedBigInteger('paiement_method_id')->nullable();
            $table->unsignedBigInteger('selected_branche_id')->nullable();
            $table->unsignedBigInteger('selected_level_id')->nullable();
            $table->unsignedBigInteger('prospecting_source_id')->nullable();
            $table->foreign('maternal_language_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('birth_city_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('birth_country_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('religion_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('paiement_method_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('selected_branche_id')->references('id')->on('branches')->onDelete('set null');
            $table->foreign('selected_level_id')->references('id')->on('levels')->onDelete('set null');
            $table->foreign('prospecting_source_id')->references('id')->on('parameters')->onDelete('set null');

            //persons
            $table->string('account_number')->nullable();
            $table->string('paypal')->nullable();
            $table->decimal('salary', 10, 2)->nullable();
            $table->decimal('hourly_rate', 10, 2)->nullable();
            $table->string('tax_number')->nullable();
            $table->date('recruitment_date')->nullable();
            $table->date('departure_date')->nullable();
            $table->text('departure_reasons')->nullable();
            $table->unsignedBigInteger('diploma_id')->nullable();
            $table->unsignedBigInteger('banque_id')->nullable();
            $table->unsignedBigInteger('function_id')->nullable();
            $table->foreign('diploma_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('banque_id')->references('id')->on('parameters')->onDelete('set null');
            $table->foreign('function_id')->references('id')->on('parameters')->onDelete('set null');

            //traking
           });



    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
